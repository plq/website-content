Title: Die Corona-Krise - Entschleunigung und Deutungshoheit
Date: 2020-03-16 23:00
Category: Artikel
Tags: Nachhaltigkeit, Postwachstum, Gesundheit
Slug: corona-postwachstum
Author: CarK
xSummary: Corona-Krise: 1. Abstand halten und Hygiene schützen Menschenleben! 2. Eine globale Rezession zeichnet sich ab. Diese Krise könnte eine der letzten Möglichkeiten sein, die rücksichtislose kapitalistische Ideologie mit absehbar fatalen Auswirkungen wirksam zu delegitimieren und den öffentlichen Diskurs auf eine an der Menschenwürde orientierte sozial-ökologische Transformation zu lenken.
img_url: /img/blog/dax1.png





**Zusammenfassung**

- COVID-19 wird viel menschliches Leid verursachen; individuelle [soziale Distanznahme] hilft!
- Es könnte eine akute und heftige Wirtschaftskrise kommen.
- Das birgt Chancen und Risiken für den Nachhaltigkeitsdiskurs.
- Anders als 2008 sollten wir die Chancen besser nutzen und klar die Systemfrage stellen.
- Wenn die Krise offiziell da ist, müssen wir vorbereitete Ideen aus der Schublade holen können.
- *Jetzt sprechfähig werden* → Kapitalistische Deutungshoheit überwinden!

[soziale Distanznahme]: https://perspective-daily.de/article/1181/cE9EHAf1

### Aktueller Stand (2020-03-16)

- Das Corona-Virus dominiert massiv den gesellschaftlichen Diskurs.
- Die Zahl der COVID-19-Infizierten wächst in vielen Ländern exponentiell.
- Für möglichst wenig Todesfälle ist eine Abflachung der Kurve extrem wichtig. Deswegen: Kontakte reduzieren, Abstand halten und Hygiene beachten!
- Die Mobilität und das öffentliche Leben werden stark eingeschränkt.
- Die Börsenkurse - als monetär ausgedrücktes Maß an Zukunftserwartung - rauschen in den Keller.
- Menschen kaufen und horten Lebensmittel und Hygieneartikel.
- Regierungen kündigen riesige öffentlich finanzierte Hilfspakete für die Wirtschaft an.

### Kurzfristige Perspektive

- Die Realwirtschaft rutscht wahrscheinlich in eine Rezession (Schrumpfung des BIP).
- Menschen sind gezwungen (und haben Zeit) darüber nachzudenken, was wirklich wichtig ist - und was nicht.

### Implikationen für die Nachhaltigkeitsbewegung (spekulativ)

- In bzw. nach der Finanzkrise 2008, ist es der humanistisch-emanzipatorischen Zivilgesellschaft nicht gelungen, die kapitalistischen Ideologie und den aus ihr resultierenden problematischen Gesellschaftsstrukturen nennenswert zu delegitimieren.
- Nach der Krise (ab 2008) wurde die Ausbeutung von Menschen und Natur trotz klar absehbarer fataler Konsequenzen ungebremst fortgesetzt.
- Die sich abzeichnende Krise 2020 sollte kommunikativ besser genutzt werden. Z.B.
    - um Hyper-Globalisierung und Hyper-Mobilität in Frage zu stellen,
    - um regionale Wertschöpfungsketten und dezentrale Erzeugung zu würdigen,
    - um die Wichtigkeit von Resilienz deutlich zu machen,
    - um Verschwendung, Gier und Verantwortungslosigkeit zu diskreditieren,
    - um Werbung und die durch sie erschaffenen absurden Konsumnormen anzugreifen,
    - um das destruktive Potenzial des Wettbewerbsprinzips zu betonen,
    - um die Ökonomisierung des Gesundheitssystems und den Just-In-Time-Ansatz zu kritisieren,
    - um exponentielles Wachstum zu problematisieren,
    - um das Narrativ des Marktes als unhinterfragbare Quasi-Gottheit zu dekonstruieren.
- Zudem sollten folgende Widersprüche deutlich gemacht werden:
    - Corona vs. Klima
        - COVID-19 betrifft überproportional ältere Menschen (aktuell) im globalen Norden. Die politischen Führungen verfügen mit Billigung der Gesellschaft zur Gefahrenabwehr drastische Einschränkungen der Wirtschaftstätigkeit.
        - Die Klimakrise betrifft dagegen überproportional junge Menschen und den globalen Süden. Die politischen Führungen sabotieren und verzögern seit Jahrzehnten echte Lösungen.
    - Markt vs. Staat
        - In guten Zeiten schimpft die Wirtschaft auf staatliche Regulation und das Erheben von Steuern: "Mehr Markt, weniger Staat!".
        - In Krisenzeiten muss der Staat mit Steuermitteln einspringen um Hilfsprogramme aufzulegen, Banken zu retten, Kurzarbeiter:innen-Geld zu bezahlen, etc.
        - Das unternehmerische Risiko wird, wo es nur geht, auf den Staat und damit die Allgemeinheit abgewälzt.
        - In Krisenzeiten wird der kapitalistische Ansatz an das Risiko-Management besonders deutlich: *Gewinne privatisieren, Verluste sozialisieren.*

### Gefahren

- Legitimationsversuche von Massenüberwachung zur Ansteckungsprävention
- Marginalisierung von Nachhaltigkeitsfragen gegenüber kurzfristiger Wirtschaftshilfe
- Entsolidarisierung in der Gesellschaft (durch quarantänebedingte Vereinzelung)

### Zwischenfazit

- Es geht um Deutungsmuster und Deutungshoheit. Ein an der Menschenwürde orientierter kapitalismuskritischer Nachhaltigkeitsdiskurs muss offen und sachlich geführt werden können.
- Die aktuelle Situation eignet sich als Chance, zentrale Begriffe der Postwachstumsbewegung wie "Entschleunigung", "Suffizienz", "Resilienz" und "Dezentralisierung" mit möglichst viel Aufmerksamkeit zu versehen.


### Deutungshoheit - Umsetzungsmöglichkeiten

- These: Der Ausfall von Veranstaltungen, Treffen etc. führt bei vielen Menschen temporär zu mehr Zeit.
- Diese könnte u.a. dazu genutzt werden, um aktiv auf den gesellschaftlichen Diskurs Einfluss zu nehmen, z.B. über
    - Persönliche Gespräche
    - Leser:innen-Briefe
    - Stellungnahmen in den sozialen Medien
    - Eine Hashtag-Kampagne
    - Aktivierung persönlicher Kontakte mit medialer Reichweite
- Eigene Erfahrung: mit direkter persönlicher Ansprache (Kontaktformular) und paralleler Veröffentlichung kann man durchaus [Erfolg](/2020/03/Verbrauchertipp-Griff-ins-Klo.html#update0314) haben.
- All das erfordert *sprechfähig* zu sein: Im richtigen Moment die passenden Argumente, idealerweise mit Beispielen und Quellen, parat zu haben und zielgruppengenau in Worte fassen zu können. Und Gegenargumente kennen und entkräften können.

### Fazit

- COVID-19 wird viel menschliches Leid verursachen; individuelle soziale Distanznahme hilft!
- Es könnte eine akute und heftige Wirtschaftskrise kommen.
- Das birgt Chancen und Risiken für den Nachhaltigkeitsdiskurs.
- Anders als 2008 sollten wir die Chancen besser nutzen.
- Wenn die Krise offiziell da ist, müssen wir vorbereitete Ideen aus der Schublade holen können.
- *Jetzt sprechfähig werden* → Kapitalistische Deutungshoheit überwinden!
