Title: Die kommende Wirtschaftskrise - Denkanstöße in der Postwachstumsblase
Date: 2020-03-22 17:00
Category: Artikel
Tags: Nachhaltigkeit, Postwachstum, Wirtschaft, Gesundheit
Slug: corona-krise-denkanstoesse
Author: CarK
xSummary: Corona-Krise: 1. Abstand halten und Hygiene schützen Menschenleben! 2. Eine globale Rezession zeichnet sich ab. Diese Krise könnte eine der letzten Möglichkeiten sein, die rücksichtislose kapitalistische Ideologie mit absehbar fatalen Auswirkungen wirksam zu delegitimieren und den öffentlichen Diskurs auf eine an der Menschenwürde orientierte sozial-ökologische Transformation zu lenken.
img_url: /img/blog/dax1.png


# „By design or by desaster“ - Denkanstöße in der Postwachstumsblase unter dem Eindruck der COVID-19-Pandemie


Die durch das Corona-Virus ausgelöste Krankheit COVID-19 hat massive Auswirkungen auf das Weltgeschehen. Zunächst unmittelbar durch ihre Folgen für die Gesundheit der Betroffenen. Weitaus mehr Menschen sind aber durch Ausgangsbeschränkungen, Schulschließungen und dem erheblichen Rückgang wirtschlaftlicher Aktivität indirekt betroffen. Der Text entsteht in der Hoffnung, dass die gesundheitlichen Folgen der Pandemie so gut es geht, unter Kontrolle gebracht werden können und betrachtet die Zeit nach den allgemeinen harten Einschränkungen des öffentlichen Lebens. Es ist absehbar, dass auf die Gesundheitskrise eine heftige Wirtschaftskrise folgen wird und dass beide Krisen zusammen die Gesellschaft in den kommenden Jahren seht stark beeinflussen werden.

Für die wachstumskritische Nachhaltigkeitsbewegung stellt sich - wie für alle anderen - die Frage: wie gehen wir damit um. Die ökologischen und sozialen Probleme die bisher unser Handeln motiviert haben, sind ja nicht weg, genau so wenig, wie die Millionen Patient:innen, die an anderen Krankheiten als COVID-19 erkrankt sind spontan gesunden, um Behandlungskapazitäten frei zu machen. Die neue Krise kommt schlicht noch oben drauf auf den ohnehin schon hohen Stapel, und wir alle müssen irgendwie damit klarkommen.

Die Postwachstumsbewegung muss dabei mit der Herausforderung umgehen, dass ihr zentraler Antagonist - das Wirtschaftswachstum - sich als Folge der Pandemie quasi über Nacht in Luft aufgelöst hat. Weil dieser Einbruch die Gesellschaft quasi unvorbereitet trifft, geht er mit harten Verwerfungen einher. Es ist deshalb zu erwarten, dass es starke sehr Bestrebungen geben wird, das "Wachstum wiederzubeleben" — auch zu Lasten anderer gesellschaftlicher Ziele.

Anhand der drei folgenden Fragen wird versucht, Denkanstöße für die aus Sicht des Autors notwendige strategischen Neuausrichtung der Postwachstumsbewegung zu liefern.


## Wie können wir konkret helfen?

Die Postwachstumsbewegung zeichnet sich vor allem durch Werte wie Solidarität, Kreativität und Improvisationskunst aus. In gesellschaftlichen Nischen wird seit Jahren ausprobiert, wie sich mit wenig Geld und möglichst unabhängig von globalen Wertschöpfungsketten gut leben lässt. Hier liegen Kompetenzen, für die in einer Krise mehr Bedarf und mehr Aufmerksamkeit besteht: Gebrauchsgegenstände reparieren, Nahrungsmittel selber anbauen und bevorraten, regionale Beschaffung, effizienter Umgang mit Energie, Ressourcen und Wohnfläche. Diese Kompetenzen werden in Krisenzeiten für deutlich mehr Menschen relevant. Nun gilt es, die entsprechenden Infos zusammenzustellen und für eine breite Zielgruppe niedrigschwellig verfügbar zu machen. Zugleich sollten die dabei entstehenden Erfahrungen für die Weiterentwicklung genutzt werden. Die Organisation der dafür notwendigen Kommunikationsprozesse mit vielen Beteiligten und heterogenen Erfahrungen, Zielvorstellungen und Kommunikationsstilen ist absehbar nicht-trivial. Potenzial bieten hier möglicherweise Erfahrungen und Werkzeuge aus dem Bereich der Entwicklung Open-Source-Software.

Auch ideell hat Postwachstum einiges im Angebot, was in Krisenzeiten sehr hilfreich sein und breitere Resonanz finden kann. Z.B. die Emanzipation von Konsum als Sinnspender und die Betonung solidarischer und respektvoller zwischenmenschlicher Beziehungen. Hinzu kommt, dass die Relativierung von Erwerbsarbeit als Anker der persönlichen Identität in der Postwachstumsbewegung eine starke Tradition hat. Auch hier kommt es darauf an, diese Perspektiven zielgruppenspezifisch aufbereitet verfügbar zu machen.

In beiden Fällen sollte hier das konkrete pragmatische Problemlösen im Vordergrund stehen und grundsätzliche Systemkritik in den Hintergrund treten und eher dezent stattfinden. Dadurch steigt die Wahrscheinlichkeit der Verbreitung des publizistischen Materials und damit die Menge der Menschen die mit Hifsangeboten und Sensibilisierung erreicht werden können. Denkbar erscheinen hier einleitend und abschließend kurze einordnende Textbausteine (bzw. audio-visuelle Äquivalente), mit Verweisen auf passende weitereführenden Informationen.


## Wie können wir uns im Krisenmodus weiter für Nachhaltigkeit einsetzen?


Aktuell wird die öffentliche Aufmerksamkeit sehr stark vom konkreten Verlauf der Pandemie bestimmt. Zu vermuten ist, dass sich in den kommenden Monaten der Fokus wieder etwas weitet, wohl aber vor allem mit Blick auf die wirtschaftlichen Folgen. Langfristige abstrakte Probleme (Nicht-Nachhaltigkeit, soziale Ungleichheit, etc.) werden es im Vergleich zur Prä-Pandemie-Ära tendenziell noch  haben, als relevant erachtet zu werden. Zumindest unausgesprochen wird die Frage im Raum stehen, ob es gerade wirklich keine dringenderen Probleme zu lösen gäbe.

Darauf kann und sollte sich die Postwachstumsbewegung vorbereiten. Zunächst kann versucht werden, konkrete Bezüge zu aktuellen Entwicklungen herzustellen, z.B. Störanfälligkeit globaler Lieferketten oder die Zoonosen-Gefahr durch Menschlichen Eingriff in Ökosysteme. Weiterhin kann der Unterschied zwischen "dringend" und "wichtig" hervorgehoben werden, z.B. mit dem Verweis darauf, dass bis Ende 2019 das Thema Pandemie-Prävention auch nicht als dringend erachtet wurde, obwohl es unzweifelhaft wichtig war. Es ist also anzustreben, thematisch eine geeignete Rahmung (Framing) zu finden, um von der öffentlichen Aufmerksamkeit nicht marginalisiert zu werden.

Die aktuelle Entwicklung zeigt, dass ein massiver Digitalisierungsschub der Gesellschaft erfolgt. Hierbei ist es wichtig, diesen möglichst nachhaltig zu gestalten, d.h. auf Kriterien wie Datensparsamkeit, Datensicherheit, Dezentralität, Quelloffenheit und Unabhängigkeit zu achten und diese auch einzufordern. Der überstürzte Umzug in die Heimarbeit und physische Distanz hat in vielen wirtschaftlichen, behördlichen und zivilgesellschaftlichen Kontexten zur Auswahl von bekannten Lösungen für die digitale Kollaboration geführt, welche diese Kriterien nicht erfüllen und z.T. problematische Geschäftsmodelle haben. Hier ist die Postwachstumsbewegung zu kritischer Selbstreflektion aufgerufen und zum Aufbau neuer Kompetenzen in den Bereichen Nutzung, Betrieb (Deployment) und Weiterentwicklung digitaler Software-Infrastruktur. Ein naheliegender zivilgesellschaftlicher Partner ist dabei die Bits&Bäume-Bewegung. Parallel zur "Nachhaltigkeitskur" für die individuelle und Engagement-Ebene bietet das Themafeld nachhaltige Gestaltung der Digitalisierung aktuell und in der absehbaren Zukunft vergleichsweise viel Anschlussfähigkeit und könnte daher verstärkt bearbeitet werden.


## Wie kann die „Systemfrage“ trotz oder gerade in der Krise im Mainstream-Diskurs thematisiert werden?

In den Medien wird derzeit nicht mit großen Worten gespart. Von einer kollektiven [traumatischen Erfahrung]() ist die Rede und davon das es nach Corona nicht so weitergehen wird, wie davor. Im Zuge der [Diskussion](https://www.deutschlandfunk.de/lindner-fdp-zu-coronaimpfstoff-da-ist-die-menschheit-als.868.de.html?dram:article_id=472553) über eine mögliche amerikanische Übernahme eines deutschen Pharma-Unternehmens hat der FDP-Chef Christian Lindner folgende bemerkenswerte Äußerung getan:

    [G]erade in einer solchen Krisenzeit verbietet es sich, unter Partnern oder überhaupt unter zivilisierten Menschen so miteinander umzugehen und einseitig den Vorteil zu suchen. Bei einer Menschheitsherausforderung ist auch die Menschheit als Ganzes gefordert. Da kann keiner auf seinen kleinen eigenen Vorteil schauen.

Hinzu kommt die Tatsache, dass jetzt in großer Einhelligkeit auf verschiedenen Ebenen öffentlich finanzierte Rettungspakete, Notfallfonds und so weiter gefordert und beschlossen werden. Im Moment stellt sich niemand mit der Forderung "Mehr Markt, weniger Staat!" auf eine diskursive Bühne. Das Gegenteil ist der Fall — wenn vielleicht auch nicht so plakativ. Die Erzählung, dass möglichst freie Märkte und möglichst ungehinderter globaler Wettbewerb eine erstrebenswerte Sache seien, muss sich jetzt grundlegende Fragen gefallen lassen. Ihrer Vertreter:innen geraten in Erklärungsnot oder machen sich lächerlich.

In bzw. nach der Finanzkrise ab 2008, ist es der humanistisch-emanzipatorischen Zivilgesellschaft nicht gelungen, die kapitalistischen Ideologie und den aus ihr resultierenden problematischen Gesellschaftsstrukturen nennenswert zu delegitimieren. Auch nach jener Krise wurde die Ausbeutung von Menschen und Natur trotz klar absehbarer fataler Konsequenzen ungebremst fortgesetzt.

Im Interesse der Menschheit sollte das bei der sich abzeichnenden Wirtschaftskrise 2020 anders laufen.

Die Aufgabe der kritischen Zivilgesellschaft muss es sein, dass Prinzip „Gewinne privatisieren, Verluste sozialisieren“ jetzt mehr denn je anzuprangern und zu delegitimieren. Der anstehende Jahrestag der von Kevin Kühnert angestoßenen Kollektivierungsdebatte böte dafür vielleicht eine erste größere Gelegenheit.

Aus Potwachstumssicht zeichnet sich ab, dass bestimmte Sichtweisen, Botschaften und Forderungen mehr Gehör und mehr Einfluss finden können, Z.B.
    - Hyper-Globalisierung und Hyper-Mobilität in Frage zu stellen,
    - regionale Wertschöpfungsketten und dezentrale Erzeugung zu würdigen,
    - die Wichtigkeit von Resilienz deutlich zu machen,
    - Verschwendung, Gier und Verantwortungslosigkeit zu diskreditieren,
    - Werbung und die durch sie erschaffenen absurden Konsumnormen anzugreifen,
    - das destruktive Potenzial des Wettbewerbsprinzips zu betonen,
    - die Ökonomisierung des Gesundheitssystems und den Just-In-Time-Ansatz zu kritisieren,
    - den Zusammenhang zwischen exponentiellem Wachstum und Systemkollaps hervorzuheben,
    - das Narrativ des Marktes als unhinterfragbare Quasi-Gottheit zu dekonstruieren.

Zudem kann folgender Widerspruch deutlich gemacht werden:  COVID-19 betrifft überproportional ältere Menschen (aktuell) im globalen Norden. Die politischen Führungen verfügen mit weitestgehender Billigung der Gesellschaft zur Gefahrenabwehr drastische Einschränkungen der Wirtschaftstätigkeit. Die Klimakrise betrifft dagegen überproportional junge Menschen und den globalen Süden. Die politischen Führungen sabotieren und verzögern seit Jahrzehnten echte Lösungen und stellen ökonomische Belange über den Schutz von Lebensgrundlagen.

Die Herausforderung im Bereich der Systemfrage besteht darin, bei aller gebotenen und notwendigen harten Kritik am Kapitalismus (und dem ihm zu Grunde liegenden rücksichtslosen Egoismus) nicht der kollektivistischen Autokratie das Wort zu reden, sondern Resilienz, Nachhaltigkeit und individuelle Selbstbestimmung zu vereinen.

Andererseits sollte auch bei der zu erwartenden schweren Wirtschaftskrise die alte [Weisheit des Kängurus]() nicht vernachlässigt werden,

    dass die meisten Krisentheorien des Kapitalismus, die den baldigen Zusammenbruch vorhersagen, daran kranken, dass sie unterschätzen, wie viele einst wertfreie Bereiche des gesellschaftlichen Zusammenlebens noch der kapitalistischen Verwertungskette anheimfallen können, um solchermaßen die Krisentendenzen durch eine quasi erneute ursprüngliche Akkumulation abzuschwächen[.]

D.h. auch wenn der Kapitalismus in eine schwere Krise gerät, heißt das nicht, dass er schon überwunden ist, und erst Recht nicht, dass es danach menschenfreundlicher wird.

## Fazit

Die Lage ist dynamisch. Während der Text entsteht ist noch nicht absehbar, wie schwer es welche Regionen treffen wird, wie lange die akute Ansteckungsgefahr vorhanden ist, welche Auswirkungen die harten Freiheitseinschränkungen auf die Gesellschaft haben werden etc. Unvermeidlich aber scheint, dass sich die ganze Gesellschaft anpassen muss und damit auch die Postwachstumsbewegung. Neben unbestreitbar viel menschlichem Leid, dass durch COVID19 direkt oder indirekt verursacht wird, und unbestreitbaren Gefahren für die humanistischen Werte gibt es aber auch Anlässe zur Hoffnung. Erstens, dass die Pandemie zeigt, dass auch demokratisch organisierte Gesellschaften grundsätzlich in der Lage sind, unbequeme wissenschaftliche Fakten zu akzeptieren und darauf mit der notwendigen Konsequenz und auch zu Lasten wirtschaftlicher Interessen zu reagieren. Und zweitens, dass die krisenbedingte Entschleunigung für eine gesamtgesellschaftliche Denkpause, inklusive Infragestellung des bisherigen Lebensstils, genutzt werden kann, aus der die Menschheit klüger hervorgeht. Klüger - sowohl im Sinne der instrumentellen Vernunft, d.h. Wie können konkrete Probleme gelöst werden?, als auch im Sinne der normativen Vernunft: Was ist uns eigentlich wirklich wichtig?.

Gerade im Hinblick auf die normative Prioritätensetzung resultiert für die Postwachstumsbewegung eine große Chance — und gleichzeitig eine große Verantwortung: Das  kapitalistische Basis-Narrativ von der selbstregulierenden Profitmaximierung zum Wohle aller, kann in naher Zukunft seine bislang praktisch unangefochtene gesellschaftliche Deutungshoheit verlieren. Darauf sollten wir vorbereitet sein und darauf sollten wir taktisch und strategisch hinwirken. Es könnte vielleicht die letzte Gelegenheit sein, die im Vergleich zur aktuellen Pandemie ungleich größere Katastrophe abzuwenden: Dem durch das exponentielle Wachstum eines destruktiven globalen Kapitalismus verursachten Kollaps der ökologischen und sozialen Systeme, die unsere Zivilisation erst möglich machen.




Postwachstum:
stellt die Selbstverständlichkeit von Wachstum
als politischem Ziel infrage und macht Vorschläge für theoretische und prakti-
sche Alternativen.



die Suche nach grundlegenden und
systemischen Alternativen zu stärken und vielfältige Akteure aus sozialen Be-
wegungen und alternativ-ökonomischen Strömungen zusammenzuführen


es geht um die Konturen eines



Links:


https://www.lia-blog.at/post/corona-einmal-anders-6-dinge-die-wir-auch-positiv-sehen-k%C3%B6nnen



https://www.forbes.com/sites/jeffmcmahon/2020/03/11/coronavirus-lockdown-may-save-more-lives-from-pollution-and-climate-than-from-virus/#5a56d9ca5764


https://www.horx.com/47-corona-eine-resilienz-uebung/

Pandemie zeigt: soziale Innovationen (Verhaltensänderungen), die durchaus technologie-gestützt sein können, wirken. Technologie alleine ist kein Wundermittel


Ausgerechnet Gesundheitskrisen bergen kulturelle Heilungspotentiale.


