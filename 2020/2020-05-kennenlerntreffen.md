Title: Offene Frage- und Kennenlernrunde (04.06., 19:00, BigBlueButton)
Date: 2020-05-26 09:30
Category: Artikel
Tags: Organisation, PLQ
Slug: kennenlerntreffen
Author: PLQ
xSummary: Um eine niedrigschwellige Möglichkeit zu bieten uns kennenzulernen und ggf. die Mitarbeit in unserer Gruppe zu vereinfachen, führen wir eine offene Frage- und Kennenlernrunde per Videokonferenz durch: 04. Juni, 19:00 Uhr.
img_url: /img/blog/open-door.jpg

# Offene Frage- und Kennenlernrunde

**Do. 04. Juni, 19:00 Uhr**

In den letzten Wochen verzeichnen wir ein verstärktes Interesse an unserer Initiative. Die Idee mit einer Parteigründung *Lebensqualität* als politisches Leitmotiv stärker in den öffentlichen Diskurs einzubringen, stößt auf Resonanz.

Daher laden wir alle interessierten Menschen zu einem virtuellen Frage- und Kennenlerntreffen ein, um gemeinsam zu diskutieren, ob und wie sich Nachhaltigkeit und Postwachstum in anschlussfähige Politik übersetzen lassen.

Das Treffen wird niedrigschwellig und datensparsam über eine selbst gehostete Instanz der freien Video-Konferenzssoftware BigBlueButton realisiert.
Bei Interesse meldet euch gerne per E-Mail bei uns:

<div style="text-align:center;">
<img src="/img/contact-mail.png" alt="Email-Adresse: kontakt at domain-der-webseite.de" title="Email-Adresse: kontakt at domain-der-webseite.de">
</div>


Weitere Infos:

- <https://plq.de>
- <https://plq.de/programm>
- <https://plq.de/blog>

