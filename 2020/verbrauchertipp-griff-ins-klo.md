Title: Journalistisches Negativbeispiel: „Sparen für die Nachkommen“
Date: 2020-03-11 21:00
Category: Artikel
Tags: Gesellschaft, Finanzmartk, Nachhaltigkeit, Journalismus
Slug: Verbrauchertipp-Griff-ins-Klo
Author: CarK
xSummary: „Enttäuschend naiv und ignorant gegenüber der Realität des 21. Jahrhunderts“ - das ist mein Urteil über den Verbrauchertipp im Deutschlandfunk vom 06.03.2020 unter der Verantwortung des Journalisten Klaus Deuse. Es geht um Geldanlagen und zwar einzig aus einer monetären Sicht. Das Thema Nachhaltigkeit wird komplett ausgeblendet - bzw. sogar absurd interpretiert.
img_url: /img/blog/industry_power_plant.jpg



(Hinweis: Der folgende Beitrag könnte Spuren von Polemik enthalten.)

„Enttäuschend naiv und ignorant gegenüber der Realität des 21. Jahrhunderts“ - das ist mein Urteil über den Verbrauchertipp im Deutschlandfunk vom 06.03.2020 unter der Verantwortung des Journalisten Klaus Deuse.

Inhaltlich geht es darum, wie Eltern und Großeltern ihren Kindern und Enkeln finanziell die Zukunft erleichtern können. Der Beitrag basiert auf der Meinung eines Finanzexperten der Verbraucherzentrale NRW. Mit der Selbstbezeichnung „Verbrauchertipp“, d.h. als Handlungsempfehlung, hat er anders als z.B. eine Nachrichtenmeldung eindeutig und offensichtlich einen normativen Charakter. Der Beitrag ist eingebettet in die Sendung Umwelt und Verbraucher, lief an einem Freitag - also dem Wochentag, den die junge Generation zum Symbol ihres Kampfes für eine lebenswerte Zukunft gemacht hat - und zwar in einer Phase, in der die talwärts rauschenden Börsenkurse die Wirtschaftsnachrichten dominieren und kurz nach dem europaweit der mit Abstand wärmste Winter seit Beginn der Aufzeichnungen zu Ende ging.

### „Nachhaltiger Zinseszins“

Statt aber diese Ankerpunkte zu nutzen, um ein solides Stück journalistischer Arbeit daran aufzuhängen, wird schlicht das altbekannte aber nach wie vor hochproblematische Loblied der Rendite zum x-ten Mal neu eingesungen. Niedrige Zinsen auf Sparbüchern werden beklagt, „chancenorientierte“ Geldanlagen in „breit aufgestellten“ Index-Fonds werden mit dem Verweis auf fünf bis sieben Prozent jährlicher Rendite als Alternative präsentiert. Das Wort „nachhaltig“ taucht zwar auf, wird aber in einem derart stumpf-monetären Sinn verwendet, dass die Entscheidung zwischen Schreien, Heulen und Erbrechen schwer fällt. Zitat: „Wie viel letztlich dabei herauskommt, das hängt auch maßgeblich von der Laufzeit ab. 'Je früher man anfängt, desto nachhaltiger können Zinseszins und Zinseffekt wirken.[…]'“. Bitte, was?? Zinseszins heißt exponentielles Wachstum und das ist das Gegenteil von Nachhaltigkeit! - Also von der richtigen Nachhaltigkeit, die besagt, dass die jeweils aktuelle Generation die Bedürfnisse zukünftiger Generationen zu respektieren hat.

Wer jedes Jahr 7% mehr als im Jahr davor aus dem Planeten herauspressen will, braucht sich nicht zu wundern, dass das zu Problemen führt. Über längere Zeiträume gibt es keine auch nur ansatzweise plausible Möglichkeit, ein solches Wachstum aufrecht zu erhalten, und die Geschichte ist reich an Beispielen von ökonomischen Zusammenbrüchen, den Versuchen, sie durch stärkere Ausbeutung von Menschen und Natur hinauszuzögern und den aus beidem resultierenden enormen Kollateralschäden.

### Qualitätsanspruch verfehlt

Dieses fundamentale Problem unserer Gesellschaftsordnung - von dem pikanterweise vor allem die „Nachkommen“ massiv betroffen sein werden, deren Begünstigung das Motiv des Verbrauchertipps darstellt - dieses Problem nicht mal mit einer müden Silbe zu erwähnen, das ist schon ein ziemlich ergiebiger Griff ins publizistische Klo. Niemand erwartet, dass ein Vier-Minuten-Verbrauchertipp die komplexen Dilemmas einer spätkapitalistischen Gesellschaft überwindet. Aber ein dezenter Hinweis auf den möglichen Widerspruch, für das eigene Enkelkind in RWE-Aktien zu investieren, während dieses mit zivilem Ungehorsam gegen die klimaschädliche Umsetzung des abstrakten Profit-Imperativs der Kapitalmärkte durch ebenjenen Konzern protestiert, hätte schon drin sein können. Oder wenigstens, wenigstens (!) die schmerzfreie Erwähnung von Banken und Geldanlagen mit Nachhaltigkeitsanspruch. (Immerhin hat die Online-Redaktion unter „Mehr zum Thema“ einen Beitrag mit Bezug dazu verlinkt.)

Ob der plakativen Ironie könnte man fast lachen, wenn es nicht so ernst wäre: Sonntags lässt man in Essay&Diskurs Matthias Greffrath und andere Intellektuelle ihre erlesene Rhetorik ausbreiten, um sich an Themen wie Demokratie, Kapitalismus, Nachhaltigkeit und ihren Interaktionen abzuarbeiten. Und werktags wird im Verbrauchertipp völlig schamlos dazu geraten, aus Renditegründen breit gestreut in MSCI-Aktien zu investieren. Junge Menschen sagen dazu: Geht's noch?

Der journalistische Qualitätsanspruch des Deutschlandfunks wird mit dem Beitrag jedenfalls nicht erfüllt.

### Hintergrundinfos

- [UN-Studie: Unternehmen verursachen Umweltschäden in Billionenhöhe](https://www.focus.de/finanzen/news/un-studie-unternehmen-verursachen-umweltschaeden-in-billionenhoehe_aid_529846.html) (Focus)
- Ulrich Petschow u.a.:[Gesellschaftliches Wohlergehen innerhalb planetarer Grenzen - Der Ansatz einer vorsorgeorientierten Postwachstumsposition](https://www.umweltbundesamt.de/publikationen/vorsorgeorientierte-postwachstumsposition) (Umweltbundesamt)
- [ Daten für Europa: Wärmster Winter seit Aufzeichnungsbeginn ](https://www.tagesschau.de/ausland/winter-europa-103.html) (Tagesschau)
- [Matthias Greffrath: Es gab ein Leben vor dem Wachstum](http://www.deutschlandfunk.de/ein-plaedoyer-fuer-politik-es-gab-ein-leben-vor-dem-wachstum.1184.de.html?dram:article_id=327610) (Beitrag für *Essay und Diskurs* im Deutschlandfunk)


<hr class="hr_gradient">


<a name="update0314"></a>
### Update (2020-03-14)

Die Redaktion des DLF hat ausführlich geantwortet und für demnächst einen Verbrauchertipp zu nachhaltigen Finanzanlagen angekündigt - auch als Reaktion auf die oben geäußerte Kritik. Zur Zeit läuft eine Anfrage an den betreffenden Redakteur, ob der Mailverkehr veröffentlicht werden kann. Inhaltlich spannend ist er auf jeden Fall.
