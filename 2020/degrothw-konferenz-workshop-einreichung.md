Title: Wie kann Postwachstum in anschlussfähige Politik übersetzt werden? - „Lebensqualität“ als politisches Leitmotiv
Date: 2019-12-28 14:00
Category: Artikel
Tags: Politik, Postwachstum, Strategie
Slug: ws-lebensqualitaet-als-aschlussfaehiges-leitmotiv
Author: CarK
xSummary: Der Begriff „Lebensqualität“ als politisches Leitmotiv hat aus unserer Sicht das Potenzial, dem bisher etablierten Cluster „Wohlstand, Konsum, Arbeitsplätze, Wachstum“ als Universalbegründung für kontra-nachhaltige Politik ausreichend inhaltliche Substanz entgegenstellen zu können, ist dabei relativ unverbraucht und bietet weniger Möglichkeit für menschenfeindliche Uminterpretation als z.B. „Freiheit“ oder „Gerechtigkeit“.
img_url: /img/blog/degrowth-snail-glass-ball-pb.jpg


**Hinweis:** Der folgenden Text wurde bei der [Degrowth-Konferenz 2020](https://www.degrowthvienna2020.org/) als Workshop eingereicht. Eine Entscheidung über die Aufnahme ins Programm steht noch aus.

---



Verbreitete Begründungen von wachstumsorientierter Politik basieren auf dem Gleichsetzen von Konsum mit Glück (bzw. „Nutzen“). Die destruktiven Implikationen fortwährenden Wachstums werden dabei häufig ignoriert und zum Teil geleugnet. Sich diesem Narrativ außerhalb geschützter Sphären entgegenzustellen kann sich anfühlen wie säkulare Blasphemie und erfordert ein hohes Maß an Selbstvertrauen. Hinzu kommt, dass viele Begriffe, die zur Beschreibung und Begründung alternativer („nachhaltiger“) Erzählungen in Frage kommen, durch bisherigen Gebrauch, Missbrauch und geziehlte Diskreditierung verschlissen oder beschädigt sind, so dass sie die für demokratische Zustimmung nötige breite Anschlussfähigkeit nicht haben. Beispiele hierfür sind „Sozialismus“, „Links“, „Grün“ und „Solidarität“.
Andere wie etwa „Freiheit“ und „Gerechtigkeit“ werden häufig auch als Legitimation problematischer Politikansätze herangezogen.

Unser Vorschlag ist daher die Etablierung von **“Lebensqualität“ als politischem Leitmotiv**. Dieser Begriff hat aus unserer Sicht das Potenzial, dem bisher etablierten Cluster „Wohlstand, Konsum, Arbeitsplätze, Wachstum“ als Universalbegründung für kontra-nachhaltige Politik ausreichend inhaltliche Substanz entgegenstellen zu können, ist dabei relativ unverbraucht und bietet weniger Möglichkeit für menschenfeindliche Uminterpretation. **Lebensqualität möchte letztlich jeder Mensch, was eine breite Anschlussfähigkeit über die Grenzen sozialer, ökonomischer und kultureller Mikrokosmen hinaus ermöglicht.** Aus dieser Position heraus lässt sich dann plausibel auf den Vorrang von Grundbedürfnissen gegenüber Luxusbegierden hin argumentieren sowie **der grundsätzliche Anspruch aller Menschen auf ein gutes Leben** thematisieren.
Dass das Prinzip der entfesselten individuellen Profitmaximierung und das daraus resultierende Wachstumsdogma diesem Anspruch entgegenstehen, ergibt sich dann quasi von selbst.


Ein im Nachhaltigkeits- und Degrowth-Sinn erstrebenswerter Bedeutungskorridor von „Lebensqualität“ muss natürlich noch ausdifferenziert werden. Zudem gilt es Taktiken zu entwickeln, wie bzw. bei welchen Gelegenheiten diese Sichtweise in den gesellschaftlichen Diskurs eingebracht werden kann, um die Widersprüchlichkeit der etablierten Narrative vor breitem Publikum offenkundig werden zu lassen und damit deren Legitimationsanspruch zu dekonstruieren. Genau darum geht es im Workshop.

Der Workshop besteht aus drei Haupt-Teilen:

1. Vorstellung der Teilnehmenden und der Grundidee „Lebensqualität als Politisches Leitmotiv“ (15min)
2. Arbeit in Kleingruppen (3-5 Personen) zu den Fragen: Was ist Lebensqualität auf individueller Ebene? Wo besteht Überschneidung mit dem Prinzip der starken Nachhaltigkeit? Wo besteht Fehldeutungs- und Missbrauchspotenzial? Durch welches Framing kann dem vorgebeugt werden? (25min + 10min Ergebnisvorstellung der Gruppen)
3. Arbeit in Kleingruppen zu den Fragen: Bei welchen Gelegenheiten und mit welchen konkreten Maßnahmen lassen sich vorherrschende wachstumsfixierte universelle Scheinbegründungen und Deutungsmuster in Politik, Medien und Kultur argumentativ herausfordern? Welche Rolle kann ein alternatives Narrativ, welches auf Lebensqualität basiert, dabei spielen? Wie sinnvoll ist die Institutionalisierung dieser Bemühungen z.B. in Form einer politischen Partei?
(25min + 10min Ergebnisvorstellung der Gruppen + 5min Reserve und Zusammenfassung.)


Die Arbeit in den Kleingruppen besteht jeweils aus moderierten und auf Postern dokumentierten Gruppendiskussionen. Als Impulsquellen stehen bei Bedarf kurze Texte mit unterschiedlichem Kontroversitätsniveau zur Verfügung.


