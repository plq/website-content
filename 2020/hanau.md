Title: Hanau – was kommt nach der Fassungslosigkeit?
Date: 2020-03-02 21:00
Category: Artikel
Tags: Gesellschaft, Krise
Slug: hanau
Author: PLQ
xSummary: Die Mordanschläge von Hanau liegen jetzt schon beinahe zwei Wochen zurück. Das Leid der Betroffenen lässt sich noch immer nicht ansatzweise in passende Worte fassen – dazu zu schweigen ist aber noch weniger eine Option.
img_url: /img/blog/kerzen.jpg




Die Mordanschläge von Hanau liegen jetzt schon beinahe zwei Wochen zurück. Das Leid der Betroffenen lässt sich noch immer nicht ansatzweise in passende Worte fassen – dazu zu schweigen ist aber noch weniger eine Option.

Die Gesellschaft wird durch dieses Ereignis schmerzlich daran erinnert, dass es in ihr gravierende Missstände gibt, von denen die Verbreitung des kalten menschenverachtenden Hasses das akuteste Symptom ist. Die grundlegenden Missstände zu überwinden, ist langfristige Generationenaufgabe. Dem akuten Hass aber muss unmittelbar der gedankliche und emotionale Nährboden aus imaginierten und konstruierten Feindbildern entzogen werden.

Es liegt in der individuellen und der gesamtgesellschaftlichen Verantwortung, gruppenbezogener Menschenfeindlichkeit in Form von verbalen und bildlichen Herabwürdigungen deutlich entgegenzutreten. So banal und abgedroschen es klingt: Die Richtschnur dafür ist und bleibt die Menschenwürde. Nicht ohne Grund steht das Bekenntnis dazu ganz am Anfang unserer Verfassung:

> „Die Würde des Menschen ist unantastbar. Sie zu achten und zu schützen ist Verpflichtung aller staatlichen Gewalt.“

Wie weit die gesellschaftliche Realität von diesem Anspruch entfernt ist, wird vor dem Hintergrund eines Extremereignisses wie den Anschlägen von Hanau besonders deutlich. Das Geschehene kann nicht ungeschehen gemacht werden. Aber wir alle haben eine Verantwortung für die Entwicklung der Zukunft.
