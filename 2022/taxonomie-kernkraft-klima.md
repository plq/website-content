Title: Kann Kernenergie die Klimakatastrophe verhindern? Ja. Ist Kernenergie wünschenswert? NEIN!
Date: 2022-01-24 20:00
Category: Artikel
Tags: Kernenergie, PLQ, Politik
Slug: Taxonomie-Kernkraft-Klima
Author: PLQ
xSummary: Die Verengung des poltischen Nachhaltigkeitsdiskurses auf die Klimakrise hat die Kernenergie scheinbar wieder diskutabel gemacht. Dabei ist es wichtig das große Ganze in den Blick zu nehmen, also neben dem Klima, u.a. auch Gesundheit und Frieden. Desalb: Lebensqualität als ganzheitliches politisches Leitmotiv!
img_url: /img/blog/grass-nuclear-waste.jpg


# Kann Kernenergie die Klimakatastrophe verhindern? Ja. Ist Kernenergie wünschenswert? NEIN!



In der aktuellen Diskussion um die *EU-Taxonomie für nachhaltige Finanzprodukte* ist die Einstufung von Kernenergie als "nachhaltig" höchst umstritten. Für manche ist diese Technologie gegenüber der weiteren Nutzung von fossilen Energieträgern das geringere Übel. Andere Menschen können sich noch gut an die harten politischen Auseinandersetzungen vergangener Jahrzehnte erinnern, als sich unter dem Atomkraft-nein-Danke-Logo Umwelt- und Friedensaktivist:innen vereint staatlicher Repression entgegensetzten. Nach dem im Jahr 2011 beschlossenen Atomausstieg war in Deutschland das Thema lange Zeit quasi vom Tisch. Spätestens in den letzten Jahren rückte die Klimakrise in das Zentrum der Nachhaltigkeitsdiskussion.


Dass die Kernenergie im Mantel vermeintlicher "Emissionsfreiheit" zurückkehren würde, war schon damals zu befürchten. Jetzt zeigt sich die Kehrseite der **Diskursverengung auf Klimakrise** und Klimaschutz: Andere Voraussetzungen für ein gutes Leben geraten aus dem Blick und grundsätzlich unnachaltige Lösungen werden scheinbar diskutabel.

Um Missverständnisse zu vermeiden: Natürlich ist ambitionierter Klimaschutz dringend geboten. Das gegenwärtige (Nicht-)Handeln auf nahezu allen Ebenen ist beschämend ignorant gegenüber den gegenwärtigen und zukünftigen Opfern der Klimakrise. Aber Klimaschutz allein bzw. kompromissloser Klimaschutz als finales Ziel, dem sich alles andere unterordnen muss, ist kein sinnvoller Denkrahmen. Konsequent – und natürlich völlig absurd! – zu Ende gedacht, würde das nämlich bedeuten, menschengemachte Treibhausgasemissionen (inklusive der Atmung) umfassend und *unverzüglich* zu beenden. Und die effizienteste Technologie dafür ist vermutlich die Kernkraft, genaugenommen luftgestützte "Ultrakurzzeitreaktoren" deren "Leistungsklasse" nicht in Megawatt sondern Megatonnen TnT-Äquivalent gemessen wird. Weltweit existieren zehntausende solche nuklearen Sprengköpfe und zugehörige Trägersysteme. Schneller und umfassender lassen sich CO2-Emissionen auf keinem anderen Weg reduzieren.


Dieses bewusst verstörende Gedankenexperiment soll deutlich machen: **Klimaschutz kann kein Selbstzweck sein**. Deswegen ist aus unserer Sicht eine zu starke Fokussierung des politischen Diskurses auf das Klimathema wie sie in Begriffen wie "Klimawahl" oder in Parteinamen wie der "Klimaallianz" zum Ausdruck kommt, problematisch: Sie **verstellt den Blick aufs große Ganze**.

Ein intaktes Klimasystem ist eine *notwendige* Voraussetzung dafür, dass Menschen ein gutes Leben führen können, aber es ist bei weitem nicht die einzige. Dazu gehören unter anderem auch die Abwesenheit von giftigen Stoffen und gefährlicher Strahlung, Verfügbarkeit mit gesunden Lebensmitteln, medizinische Versorgung und vieles mehr.

Eine nüchterne Betrachtung der gegenwärtigen Landschaft der etablierten politischen Parteien und Akteur:innen lässt nirgendwo ein *Gesamtkonzept* erkennen, was die absehbaren großen Menschheitsherausforderungen angemessen berücksichtigt und noch dazu grundsätzlich für weite Bevölkerungsteile anschlussfähig wäre.


Deshalb plädieren wir – die [PLQ](https://plq.de) – dafür, **_Lebensqualität_ ins Zentrum politischer Überlegungen** zu setzen, an Stelle von *Wirtschaftswachstum*, *Klimaschutz*, *sozialer Gerechtigkeit*, *Freiheit* und anderen vermeintlichen Leitbegriffen. Das Ziel muss lauten: **Lebensqualität für alle gegenwärtig und zukünftig lebende Menschen, unabhängig von der Weltregion, in der sie leben**. Ausgehend von diesem Ziel wird die Absurdität viele Politikbegründungen sofort offensichtlich: "Rüstungsindustrie sichert Arbeitsplätze", "Lärmschutzregeln kosten Wachstum", "Menschrechtesauflagen gefährden Wettbewerbsfähigkeit" – ja und? Wachstum, Arbeitsplätze und Wettbewerbsfähigkeit sind – wie Klimaschutz – keine sinnvollen Ziele in sich.

Natürlich lässt sich darüber debattieren, was "Lebensqualität" ausmacht und z.B. welche individuellen Freiheiten davon erfasst sind. Doch trotz aller Unschärfe taugt der Begriff als **politisches Leitmotiv zu Betrachtung des großen Ganzen** besser, als alles was sonst bisher im Angebot ist.

Abschließend nochmal zurück zur Kernenergie und ihrer sogenannten "zivilen Nutzung": Deren Potenzial nennenswert zu einer klimafreundlichen Energieversorgung beizutragen hängt von den Einsatzbedingungen ab. Entweder sie erfolgt sehr stark reguliert, dann sind Bau, Betrieb, Rückbau und Entsorgung nicht wirtschaftlich. Oder es werden (unverantwortliche!) Regulierungskompromisse eingegangen. Dann ist die Wahrscheinlichkeit weiterer katastrophaler Störfälle untragbar hoch. Hinzu kommt, dass Kernenergienutzung neben immenser Unfallgefahr im Betrieb insgesamt noch drei weitere Problemfelder aufweist: Massive Umwelt- und Gesundheitsprobleme bei der Rohstoffgewinnung (oft im globalen Süden), immense Risiken und Kosten für die Endlagerung sowie die permanete Gefahr militärischer oder paramilitärischer Missbrauch sogenannter ziviler nuklearer Infrastruktur.

Der französische Präsident sagte 2020 zum letzten Punkt ganz offen: "Sans nucléaire civil, pas de nucléaire militaire" - "Ohne zivile Kernkraft gibt es keine militärische Kernkraft" (Quellen: [lemonde.fr](https://www.lemonde.fr/idees/article/2020/12/21/nucleaire-pour-emmanuel-macron-c-est-la-filiere-militaire-qui-prime_6064052_3232.html), [heise.de](https://www.heise.de/tp/features/Atomkraft-Ausbau-in-Frankreich-Ohne-zivile-Kernenergie-keine-militaerische-Nuklearmacht-6219628.html?seite=all)).


**Fazit**: Hypothetisch denkbar wäre, mit Hilfe von Kerspaltungstechnologie einen Klimaschutzbeitrag zu leisten. Jede auch nur halbwegs vernünftige Betrachtung der Menschheitsinteressen muss diese *nukleare Option* jedoch **ausschließen**. Jeder Euro der in Erhalt oder Neubau von nuklearer Infrastruktur fließt, fehlt beim Aufbau eines wirklich nachhaltigen Ernegiesystems basierend auf Erneuerbaren und Speichern.

