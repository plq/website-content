Title: BGE
Date: 2017-12-10 19:00:00
Author: PLQ Dresden
save_as: bge.html

<section id="Standpunkt">
    <h1>Partei für Lebensqualität</h1>
<a name="standpungkt-bge" class="shifted_anchor"></a>
<h3>Standpunkt zum bedingungslosen Grundeinkommen</h3>

<p>
Wir sind gerade dabei, unseren Standpunkt zum Bedingungslosen Grundeinkommen auszuarbeiten.
Wir halten ein BGE prinzipiell für finanzierbar und teilen die angestrebten Ziele der verschiedenen BGE-Konzepte (Überwindung von Existenzsorgen; Ermöglichung von Freiräumen für Kreativität, Sorge- und Reproduktionsarbeit sowie politisches und kulturelles Engagement). Allerdings sehen wir auch mögliche grundlegende Probleme, die wir in Form der folgenden (provokanten) Thesen formuliert haben. Derzeit suchen wir Argumente, welche diese Bedenken entkräften.
</p>
<ol>
<li>
Mit dem BGE und seiner Finanzierung sind starke Preisschwankungen für Arbeit und Produkte zu erwarten. Niemand kann sicherstellen, dass die gesellschaftlich notwendigen Berufe noch in ausreichendem Maße ausgeübt werden. Es besteht die Gefahr der Destabilisierung der gesamten Gesellschaft (Gesundheitsversorgung, Bildung, Ernährung, Justiz, Energieversorgung, innere Sicherheit). Die Kaufkraft vieler Menschen könnte sich mit einem BGE außerdem deutlich verringern, was zu einer weiteren Vergrößerung der Schere zwischen Arm und Reich führen würde.
</li>
<li>
Das BGE allein führt nicht notwendigerweise zu einer höheren Lebensqualität. Zusätzlich zu einem BGE wären große Investitionen in das Gesundheits-, Pflege- und Bildungssystem sowie in den Bereichen Justiz, Verkehr und innere Sicherheit notwendig. Wie sollen diese neben dem BGE finanziert werden? Zudem basieren die bisherigen Finanzierungsmodelle auf dem Status Quo, d. h. auf dem gegenwärtigen Preis- und Lohngefüge und sind damit nur von sehr eingeschränkter Aussagekraft.
</li>
<li>
Wenn das BGE durch Konsumsteuern finanziert wird, dann wird es gesamtgesellschaftlich noch schwieriger, eine Reduktion des Konsums zu erreichen, welche aus ökologischen Gründen dringend geboten ist. Mit anderen Worten: Eine Reduktion des Konsums würde dem BGE die Finanzierungsgrundlage entziehen.
</li>
<li>
Ein Grundeinkommen, das bedingungslos an alle in Deutschland lebenden Personen ausgezahlt würde, wäre mit einem erheblichen ökonomischen Anreiz für den Zuzug von Menschen inner- und außerhalb der EU verbunden. Die damit verknüpften Fragen zu Migration und/oder Grenzsicherung werden von den Befürworter*innen des BGE kaum betrachtet.
</li>
<li>
Ein BGE könnte der Gleichberechtigung der Geschlechter zuwiderlaufen, indem es traditionelle Rollenverteilungen stärkt (Mann: Hauptverdiener; Frau: Sorge-, Pflege- und Hausarbeit). Wenn sich die kulturellen Geschlechternormen nicht ändern, ist das BGE nicht viel mehr als eine "Herdprämie" für Frauen. Diese werden weiterhin den Großteil der Sorge-, Pflege- und Hausarbeit verrichten.
</li>
<li>
Die angestrebten Ziele können prinzipiell auch ohne ein BGE umgesetzt werden, z. B. durch Reform der Grundsicherung (HartzIV), Erhöhung des Mindestlohns, Arbeitszeitverkürzung, Steuerreform zur Begünstigung von gemeinnütziger Arbeit.
</li>
</ol>

<p>
Wir sind uns der Komplexität der Fragen bewusst, würden uns aber dennoch über Input zu den Thesen sowie andere kompetente Kontakte sehr freuen. Entweder <a href="index.html#mail">per E-Mail</a> oder gleich hier im Formular.
</p>
<hr>
</section>
<section id="feedback-section">
<a name="feedback" class="shifted_anchor"></a>
    <h2>Sag uns Deine Meinung</h2>

    <form action="scripts/process.php" method="post">

            <input name="meta-type" type="hidden" value="opinion-form" />

            Text (max. 5000 Zeichen):<br>
            <textarea name="field2" cols="50" rows="8">Ich denke ... </textarea>
            <br>
            <br>

            E-Mail-Adresse (zum Antworten, optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>

            <input type="submit" name="submit" value="Absenden">
    </form>

</section>


<!-- the following opening tag serves to fix a strange behavior in pelican -->
<p>
