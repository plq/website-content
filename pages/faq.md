Title: Kernforderungen
Date: 2021-01-25 17:30:00
Author: PLQ Dresden
save_as: faq.html
status: hidden


<!-- Diese Markdowndatei wird vom Website-Bau-Programm "zerhackt" und dort eingefügt wo wir die häufig gestellten Fragen wollen.
Als Trennmarkierung fungiert der als nächstes folgende spezielle Kommentar (`< !-- ++++++++++++ -- >`):

Die Überschrift (beginnend mit ##) bezeichnet die "Klappbox-Überschrift" und ggf. den Slider-Text.
Alles Unter der Überschrift kommt in den Aufklappbaren Teil.

Kernforderungen tauchen nur im Slider auf, wenn sie einen Kommentar `< !-- slider: True -- >` enthalten.


Die Option `slug` ist eine Zeichenkette (alphanumerisch + "_") die für den direktlink auf eine Forderung benutzt wird.


Gendersternchen muss mit "\*" geschrieben werden, weil es sonst von Markdown als Syntaxelement für *kursiven* Text aufgefasst wird


-->


<!-- ++++++++++++ -->

#### Kann man Euch schon wählen?
<!-- slider: False -->
<!-- slug: schon_waehlbar -->

Noch nicht. Im Moment bereiten wir die offizielle Parteigründung vor. Danach geht es ans Sammeln der für eine Wahlteilnahme notwendigen Unterstützungsunterschriften. Wir freuen uns aber jederzeit über [Feedback](#feedback),  [Unterstützung](#unterstuetzen) und [neue Mitglieder](#mitmachen).



<!-- ++++++++++++ -->

####  Gibt es nicht schon genug Parteien?
<!-- slider: False -->
<!-- slug: nicht_schon_genug_parteien -->

Die großen Parteien konzentrieren sich zu sehr auf Machtpolitik statt auf überzeugende Inhalte und haben deshalb erheblich an Vertrauen eingebüßt. Die kleinen sind entweder völlig unbekannt oder thematisch sehr eingeschränkt. Außerdem ist der Aufwand, in einer existierenden Partei gegen (besitzstandswahrende) Widerstände eigene Ideen zu etablieren, nicht zu vernachlässigen. Zum Glück können auch kleine Parteien durch geschickte Öffentlichkeitsarbeit die politische Agenda beeinflussen: Unser Anliegen ist es, "Lebensqualität" als politisches Leitziel zu etablieren – das ist uns wichtiger, als Macht oder Ämter zu ergattern.

Für Kooperationen mit anderen Parteien sind wir bei inhaltlicher Übereinstimmung aber offen.


<!-- ++++++++++++ -->

####  Was genau ist am gegenwärtigen politischen System schlecht?
<!-- slider: False -->
<!-- slug: was_genau_ist_gegenwaertig_schlecht -->

Verglichen mit früheren Generationen oder anderen Weltregionen geht es den meisten Menschen in Mitteleuropa ziemlich gut. Das liegt neben dem technischen Fortschritt vor allem an eingiermaßen funktionierender staatlicher Organisation.

Aber wir, bzw. die reichen Länder, generieren unseren Wohlstand zu Lasten von Menschen in anderen Weltregionen. Das ist schlicht ungerecht und birgt darüber hinaus auch massive Sicherheitsrisiken.

Außerdem sind unsere zivilisatorischen Errungenschaften gefährdet, z.B. durch ökologische Krisen, Rohstoffknappheit, soziale Spaltung, Erstarken des Totalitarismus, ...

Um diese Probleme zu überwinden, brauchen wir anschlussfähige, nachvollziehbare und konstruktive Politikangebote.




<!-- ++++++++++++ -->

####  Ist es nicht naiv anzunehmen, dass man selber irgendwas zum Positiven verändern kann?
<!-- slider: False -->
<!-- slug: positive_veraenderungen_moeglich? -->


Bei realistischer Betrachtung des Zeitgeschehens ist es stattdessen deutlich naiver zu glauben, dass die Dinge schon von alleine gut ausgehen werden. Die Geschichte hat gezeigt: Veränderungen zum Guten sind möglich, wenn sich Menschen engagieren! Deshalb: [Mach mit!](#mitmachen)


<!-- ++++++++++++ -->

####  Reicht es nicht, ab und zu eine Online-Petition "zu unterschreiben"?
<!-- slider: False -->
<!-- slug: positive_veraenderungen_moeglich? -->



Online-Petitionen können die öffentliche Aufmerksamkeit auf eine bestimmten Missstand lenken und damit politischen Druck aufbauen. Aber das alleine reicht in den seltensten Fällen. Um wirklich etwas zu verändern, braucht es kontinuierliche politische Arbeit. Und genau dafür gibt es Parteien.






