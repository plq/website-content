Title: Memes
Date: 2017-11-05 18:49:00
Author: PLQ Dresden
save_as: memes.html

<section id="Programm">
    <h1 style="margin-top:10px; margin-bottom:0px;">Memes zum PLQ Grundsatz&shy;programm 2019</h1>
<p>
    Im Juli 2019 haben wir unser erstes Grundsatzprogramm fertig gestellt (<a href="programm.html">Online-Version</a>, <a href="downloads/Parteiprogramm_2019.pdf">PDF-Download</a>). Die wichtigsten Grundaussagen haben wir in folgenden Memes grafisch aufbereitet.

    <br>
    <br>
</p>

<p>
    <a name="01_Alternativlos" class="shifted_anchor"></a>
    <a href="img/memes/01_Alternativlos.jpg">
    <img class="box_shadow" src="img/memes/prev/01_Alternativlos.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#01_Alternativlos">Direktlink zu diesem Meme</a>
    </span>

</p>

<hr class="hr_gradient">

<p>
    <a name="02_Faritrade" class="shifted_anchor"></a>
    <a href="img/memes/02_Faritrade.jpg">
    <img class="box_shadow" src="img/memes/prev/02_Faritrade.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#02_Faritrade">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="03_OekoTransformation" class="shifted_anchor"></a>
    <a href="img/memes/03_OekoTransformation.jpg">
    <img class="box_shadow" src="img/memes/prev/03_OekoTransformation.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#03_OekoTransformation">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="04_Jobcenter" class="shifted_anchor"></a>
    <a href="img/memes/04_Jobcenter.jpg">
    <img class="box_shadow" src="img/memes/prev/04_Jobcenter.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#04_Jobcenter">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="05_Anwachsen" class="shifted_anchor"></a>
    <a href="img/memes/05_Anwachsen.jpg">
    <img class="box_shadow" src="img/memes/prev/05_Anwachsen.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#05_Anwachsen">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="06_Asylantrag" class="shifted_anchor"></a>
    <a href="img/memes/06_Asylantrag.jpg">
    <img class="box_shadow" src="img/memes/prev/06_Asylantrag.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#06_Asylantrag">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="07_Schule" class="shifted_anchor"></a>
    <a href="img/memes/07_Schule.jpg">
    <img class="box_shadow" src="img/memes/prev/07_Schule.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#07_Schule">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="08_Ueberwachung" class="shifted_anchor"></a>
    <a href="img/memes/08_Ueberwachung.jpg">
    <img class="box_shadow" src="img/memes/prev/08_Ueberwachung.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#08_Ueberwachung">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="09_Wachstum1" class="shifted_anchor"></a>
    <a href="img/memes/09_Wachstum1.jpg">
    <img class="box_shadow" src="img/memes/prev/09_Wachstum1.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#09_Wachstum1">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">

<p>
    <a name="10_Lebensqualitaet" class="shifted_anchor"></a>
    <a href="img/memes/10_Lebensqualitaet.jpg">
    <img class="box_shadow" src="img/memes/prev/10_Lebensqualitaet.jpg" width="100%">
    </a>

    <span class="right_align_link">
        <a href="programm.html">Grundsatzprogramm </a>
        |
        <a href="#10_Lebensqualitaet">Direktlink zu diesem Meme</a>
    </span>


</p>

<hr class="hr_gradient">


<p>
<em>Ihr habt eine Meinug zu diesen Bildern? Schreibt uns an:</em><br><br>
    <img src="./img/contact-mail.png" alt="Email-Adresse: kontakt at domain-der-webseite.de">

<br><br>
<em>oder direkt hier:</em><br>


    <form action="scripts/process.php" method="post">

            <input name="meta-type" type="hidden" value="opinion-form" />


            Text (max. 5000 Zeichen):<br>
            <textarea name="field2" cols="50" rows="12">Ich denke ... </textarea>
            <br>
            <br>

            E-Mail-Adresse (zum Antworten, optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>

            <input type="submit" name="submit" value="Absenden">
    </form>

</p>

</section>


<!-- the following opening tag serves to fix a strange behavior in pelican -->
<p>
