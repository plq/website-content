Title: PLQ Satzung
Date: 2021-08-27 10:16:00
Author: PLQ Gründungsteam
save_as: satzung.html
Template: basic_page



# Satzung der Partei für Lebensqualität (PLQ)

Version: 1.0.0
<br>
Beschlossen am: `01.11.2021`

<!--git commit: 78f4038-->


## § 1 Ziel, Name, Sitz und Tätigkeitsgebiet

1.1 Die Partei für Lebensqualität ist eine Partei im Sinne des Grundgesetzes der Bundesrepublik Deutschland und des Parteiengesetzes. Ziel der Partei ist es, Lebensqualität zum zentralen Leitmotiv der Politik zu machen. Unter "Lebensqualität" verstehen wir die Möglichkeit, ein gutes, erfülltes Leben zu führen – und zwar für alle Menschen, auch in anderen Teilen der Welt und einschließlich zukünftiger Generationen. Die Partei für Lebensqualität bietet allen Menschen – unabhängig von Geschlecht, Hautfarbe, Nationalität, ethnischer oder sozialer Herkunft, Weltanschauung, sexueller Orientierung, körperlicher oder geistiger Gestalt – Raum für Diskussion und aktive Mitgestaltung. Wir streben einen konstruktiven und respektvollen Dialog an, sowohl parteiintern als auch nach außen. Dieser Anspruch gilt insbesondere auch für die Kommunikation bezogen auf und gerichtet an Menschen anderer politischer Überzeugungen. Gruppenbezogene Menschenfeindlichkeit und antidemokratische Bestrebungen lehnen wir entschieden ab.

1.2 Die Bundespartei führt den Namen "Partei für Lebensqualität". Die offizielle Abkürzung lautet PLQ.

1.3 Der Sitz der Partei ist Dresden.

1.4 Das Tätigkeitsgebiet der Partei ist die Bundesrepublik Deutschland.

1.5 Gebietsverbände tragen den Namen PLQ mit dem Zusatz des jeweiligen Gebietsnamens.


## § 2 Aufnahme und Austritt der Mitglieder

2.1 Mitglied der Partei kann jede natürliche Person werden, die das 14. Lebensjahr vollendet hat und die Grundwerte sowie die Satzung der PLQ anerkennt. Voraussetzung für die Mitgliedschaft ist die Abgabe einer schriftlichen Verpflichtungserklärung, die Grundwerte der PLQ einzuhalten.

2.2 Die Partei führt ein zentrales Mitgliederverzeichnis.

2.3 Personen, die Mitglied einer Organisation sind, die sich gegen die Menschenrechte oder gegen eine demokratische, pluralistische Gesellschaft richtet, können nicht Mitglied bei der PLQ werden. Wenn Mitglieder nach ihrem Eintritt in die Partei einer dieser Organisationen beitreten oder eine bestehende Mitgliedschaft in einer dieser Organisationen nachträglich bekannt wird, ist dies ein zwingender Ausschlussgrund. Der Bundesparteitag kann eine Unvereinbarkeitsrichtlinie beschließen, die Näheres regelt und eine Liste mit Organisationen enthält, die als unvereinbar gelten. Der Bundesvorstand kann diese Liste per Beschluss vorläufig ändern. Der folgende Bundesparteitag entscheidet über die Annahme oder Ablehnung.

2.4 Die Mitgliedschaft wird beim Gebietsverband der niedrigsten Gliederungsebene beantragt, in dessen Zuständigkeitsgebiet das zukünftige Mitglied seinen Erstwohnsitz hat. Der Aufnahmeantrag ist in elektronischer oder schriftlicher Form zu stellen. Über die Aufnahme entscheidet der Vorstand des Gebietsverbands innerhalb von vier Wochen nach bestätigtem Eingang des Aufnahmeantrags. Ist dem Vorstand im Einzelfall aus wichtigem Grund keine Entscheidung innerhalb der vorgenannten Frist möglich, verlängert sich diese um weitere zwei Wochen. Hierüber ist der\*die Bewerber\*in unverzüglich schriftlich zu benachrichtigen. Eine Ablehnung sollte, aber muss nicht begründet werden. Im Mitgliedsantrag muss vollständige Auskunft über aktuelle und frühere Mitgliedschaften in Parteien und sonstigen politischen Gruppierungen gegeben werden. Bei parallelen Parteimitgliedschaften muss schriftlich erläutert werden, wie Interessenskonflikte von der jeweiligen Person vermieden werden. Unvollständige oder unrichtige Auskünfte sind je nach Schwere mit Parteiordnungsmaßnahmen gemäß § 4 zu ahnden.

2.5 Jedes Mitglied gehört grundsätzlich den Gebietsverbänden an, in deren Zuständigkeitsgebieten es seinen Erstwohnsitz hat. Befindet sich der Erstwohnsitz im Ausland, wird das Mitglied grundsätzlich dem Bundesverband zugeordnet. Jedes Mitglied hat einen Wechsel des Erstwohnsitzes unverzüglich dem Bundesvorstand anzuzeigen.

2.6 Jedes Mitglied kann beim Bundesvorstand beantragen, einem anderen Gebietsverband zugeordnet zu werden, wenn der Vorstand dieses Gebietsverbands kein Veto einlegt. Ein Wechsel des Gebietsverbands wird jeweils zum neuen Kalenderjahr wirksam.

2.7 Jedem Mitglied wird für die Zeit der Mitgliedschaft eine E-Mail-Adresse zur Verfügung gestellt. Einladungen, Rechnungen sowie alle weiteren Anschreiben werden dem Mitglied an diese Adresse zugestellt. Ein Versand per Post erfolgt nicht.

2.8 Die Mitgliedschaft endet durch Austritt, Ausschluss oder Tod. Bereits gezahlte Mitgliedsbeiträge werden nicht erstattet. Der Austritt ist dem Gebietsverband der niedrigsten Gliederungsebene sowie der Bundespartei schriftlich anzuzeigen.


## § 3 Rechte und Pflichten der Mitglieder

3.1 Jedes Mitglied hat das Recht im Rahmen dieser Satzung und der Satzung seiner Gebietsverbände, die Ziele der PLQ zu fördern und sich an der politischen und organisatorischen Arbeit der PLQ zu beteiligen. Jedes Mitglied hat das Recht, an Wahlen und Abstimmungen im Rahmen der Satzung teilzunehmen. Alle Mitglieder der PLQ haben gleiches Stimmrecht. Die Ausübung des Stimmrechts sowie die Wahl in den Vorstand ist nur möglich, wenn die betreffende Person Mitglied des jeweiligen Gebietsverbands ist.

3.2 Jedes Mitglied ist jederzeit zum sofortigen Austritt aus der Partei berechtigt.

3.3 Jedes Mitglied ist verpflichtet, die PLQ-Grundwerte einzuhalten.

3.4 Interne Informationen können per mehrheitlichem Beschluss im betreffenden Gremium als Verschlusssache deklariert werden. Über Verschlusssachen ist Verschwiegenheit zu wahren. Verschlusssachen können per mehrheitlichem Beschluss von diesem Status befreit werden.


## § 4 Ordnungsmaßnahmen gegen Mitglieder

4.1 Verstößt ein Mitglied gegen die Satzung oder Grundwerte der Partei, kann der Vorstand eines zuständigen Gebietsverbandes oder der Bundesvorstand folgende Ordnungsmaßnahmen verhängen:
- Verwarnung
- Verweis (zeitweiliger Ausschluss von parteiinternen Kommunikationsplattformen und Parteiveranstaltungen)
- Enthebung von einem Parteiamt
- Aberkennung der Fähigkeit ein Parteiamt zu bekleiden für bis zu 2 Jahre
- Aberkennung der Mitgliedsrechte für bis zu zwei Jahre
- Antrag auf Ausschluss an das zuständige Schiedsgericht

4.2 Der Vorstand muss dem Mitglied vor dem Beschluss der Ordnungsmaßnahme eine Anhörung gewähren. Der Beschluss ist dem Mitglied unter Angabe von Gründen in Schriftform mitzuteilen. Gegen verhängte Ordnungsmaßnahmen kann beim zuständigen Schiedsgericht Einspruch eingelegt werden.

4.3 Ein Mitglied kann nur dann ausgeschlossen werden, wenn es vorsätzlich gegen die Satzung oder erheblich gegen die Grundwerte der PLQ verstößt.


## § 5 Ordnungsmaßnahmen gegen Gebietsverbände

5.1 Verstößt ein Gebietsverband schwerwiegend gegen die Grundwerte der PLQ oder weigert sich, begründete Beschwerden aufzugreifen und an ein Schiedsgericht heranzutragen, kann der Vorstand eines übergeordneten Gebietsverbands (z.B. der Bundespartei) folgende Ordnungsmaßnahmen verhängen:
- Amtsenthebung einzelner Vorstandsmitglieder
- Amtsenthebung des gesamten Vorstands
- Auflösung
- Ausschluss

5.2 Als schwerwiegender Verstoß gegen die Ordnung und die Grundwerte der Partei ist es zu werten, wenn die Gebietsverbände die Bestimmungen der Satzung dauerhaft und/oder wiederholt missachten, Beschlüsse übergeordneter Parteiorgane nicht durchführen oder in wesentlichen Fragen gegen die politische Zielsetzung der Partei handeln.

5.3 Die Mitglieder-/Delegiertenversammlung des die Ordnungsmaßnahme aussprechenden Gebietsverbandes hat die Ordnungsmaßnahme am nächsten Parteitag mit einfacher Mehrheit zu bestätigen, ansonsten tritt die Maßnahme außer Kraft. Gegen die Ordnungsmaßnahme durch einen Vorstand ist innerhalb eines Monats nach Aussprechen der Ordnungsmaßnahme die Anrufung des nach der Schiedsgerichtsordnung zuständigen Schiedsgerichtes zuzulassen.


§ 6 Allgemeine Gliederung der Partei

6.1 Die PLQ gliedert sich entsprechend der Bundesländer in Landesverbände. Existiert in einem Bundesland kein Landesverband, übernimmt der Bundesverband die Verwaltung.

6.2 Innerhalb der Landesverbände ist die Ausgestaltung weiterer Untergliederungen den Landesverbänden selbst überlassen. Grundsätzlich sind alle Gebietsverbände entlang der Verwaltungsgrenzen zu organisieren.

6.3 Jeder Gebietsverband kann in seinem Gliederungsbereich thematische Arbeitsgruppen gründen.

6.4 Alle Gebietsverbände sind an die Satzung sowie die Finanz- und Schiedsgerichtsordnung des Bundesverbands sowie die PLQ-Grundwerte gebunden. Eine Orientierung an den PLQ-Entscheidungsfindungsregeln wird ausdrücklich empfohlen. Die Gebietsverbände können ihre Angelegenheiten durch eigene Satzungen regeln, soweit die Satzung des jeweils nächsthöheren Gebietsverbandes hierüber keine Vorschriften enthält. Landessatzungen und die Satzungen der Untergliederungen der Landesverbände können ergänzende Regelungen enthalten, soweit diese der Bundessatzung nicht widersprechen. Im Konfliktfall gilt die Bundessatzung.

6.5 Organe der Partei sind die Mitglieder-/Delegiertenversammlungen und die jeweiligen Vorstände der Gebietsverbände.


## § 7 Mitglieder- und Delegiertenversammlungen

7.1 Parteitage und Aufstellungsversammlungen der Bundespartei sowie aller Gebietsverbände finden bis zu einer Mitgliederzahl von 250 Personen als Mitgliederversammlungen statt. Bei mehr als 250 Mitgliedern im jeweiligen Gebietsverband finden Parteitage und Aufstellungsversammlungen in Form einer Delegiertenversammlung statt.

7.2 Die Mitglieder-/Delegiertenversammlung ist das oberste Organ des jeweiligen Gebietsverbandes. Sie soll jährlich, mindestens jedoch alle zwei Jahre abgehalten werden.

7.3 Die Einberufung erfolgt aufgrund eines Vorstandsbeschlusses oder wenn die Mehrheit der Mitglieder des jeweiligen Gebietsverbands die Einberufung beantragt. Der Vorstand lädt jedes Mitglied mindestens acht Wochen vor Beginn der Veranstaltung per E-Mail ein. Für die Erreichbarkeit per E-Mail ist das Mitglied selbst zuständig. Grundsätzlich beinhaltet die Einladung Angaben zum Tagungsort, zum Tagungsbeginn, zur voraussichtlichen Tagungsdauer, zur vorläufigen Tagesordnung und die Aufforderung, Anträge an die Mitglieder-/Delegiertenversammlung bis spätestens sechs Wochen vor Veranstaltungsbeginn einzureichen. Spätestens eine Woche vor der Versammlung sind die Tagesordnung in aktueller Fassung, die geplante Tagungsdauer und alle Anträge, die das Mindestquorum (siehe § 7.4) erreicht haben, an alle Mitglieder per E-Mail zu kommunizieren.

7.4 Anträge an die Mitglieder-/Delegiertenversammlung:
(1) Jedes Mitglied ist auf allen Gliederungsebenen antragsberechtigt, zu denen das Mitglied gehört. Der jeweilige Antrag ist an den zuständigen Gliederungsvorstand spätestens sechs Wochen vor der Mitglieder-/Delegiertenversammlung zu richten.
(2) Der Gliederungsvorstand (bzw. eine durch ihn eingesetzte Antragskommission) sorgt dafür, dass alle eingegangenen Anträge in ihrem vollständigen Wortlaut für alle Mitglieder einsehbar sind. Gleichzeitig werden alle Anträge auf die folgenden Bedingungen geprüft: Antragsberechtigung, Vereinbarkeit mit den PLQ-Grundwerten und rechtliche Zulässigkeit. Anträge, die den genannten Kriterien nicht entsprechen, werden als unzulässig markiert. Einsprüche gegen die Entscheidungen des Vorstands bzw. der Antragskommission sind beim zuständigen Schiedsgericht möglich.
(3) Spätestens vier Wochen vor der Mitglieder-/Delegiertenversammlung beginnt die Abstimmung über alle zulässigen Anträge. An dieser Abstimmung dürfen alle Mitglieder des jeweiligen Gebietsverbands teilnehmen. Ein Antrag, der auf der Mitglieder-/Delegiertenversammlung besprochen wird, muss von mindestens 25 % der Abstimmenden (Quorum) als wichtig erachtet werden.
(4) Anträge, die zwei Wochen vor der Mitglieder-/Delegiertenversammlung das nötige Quorum erreicht haben, werden auf die Tagesordnung für die Versammlung gesetzt.
(5) Anträge, die zwei Wochen vor der Mitglieder-/Delegiertenversammlung das nötige Quorum nicht erreicht haben, werden nicht auf die Tagesordnung für die Versammlung gesetzt. Sie können zu einem späteren Zeitpunkt neu gestellt werden.
(6) Unabhängig vom Abstimmungsergebnis der Mitglieder kann der jeweils zuständige Gliederungsvorstand Anträge aus dem Diskussions-/Auswahlforum auf die Tagesordnung für die Versammlung setzen.
(7) Alle Anträge müssen die obigen Schritte durchlaufen.

7.5 Die Mitglieder-/Delegiertenversammlung wählt den Vorstand des jeweiligen Gebietsverbands. Jeder Vorstand besteht aus drei, fünf oder sieben Mitgliedern – darunter Vorsitzende\*r, stellvertretende\*r Vorsitzende\*r und Schatzmeister\*in. Über die genaue Anzahl der Vorstandsmitglieder entscheidet die Mitglieder-/Delegiertenversammlung des jeweiligen Gebietsverbands. Die Mitglieder-/Delegiertenversammlung der Bundespartei und der Landesverbände wählt außerdem die Mitglieder des jeweiligen Schiedsgerichts.

7.6 Die Mitglieder-/Delegiertenversammlung eines jeden Gebietsverbands nimmt jährlich, mindestens jedoch alle zwei Jahre, einen schriftlichen Tätigkeitsbericht des Vorstands entgegen und entscheidet über die Entlastung der Vorstandsmitglieder. Der Tätigkeitsbericht enthält einen Finanzbericht, der Auskunft über die Herkunft und Verwendung der Mittel sowie über das Vermögen der Partei gibt.

7.7 Abstimmungen über Anträge werden mit einfacher Mehrheit der Abstimmenden getroffen. Eine Änderung an Satzung, Finanz- oder Schiedsgerichtsordnung bedarf einer ¾-Mehrheit der Abstimmenden. Alle Änderungen an Satzung, Finanz- oder Schiedsgerichtsordnung müssen spätestens vier Wochen nach der beschlossenen Änderung allen Mitgliedern per E-Mail kommuniziert und auf der Webseite des Gebietsverbands veröffentlicht werden. Änderungen der PLQ-Grundwerte sowie Anträge auf Auflösung oder Verschmelzung mit einer anderen Partei benötigen eine ¾-Mehrheit der Abstimmenden eines Bundesparteitages.

7.8 Über die Ergebnisse von Mitglieder-/Delegiertenversammlungen ist ein Ergebnisprotokoll anzufertigen und an geeigneter Stelle auf der Webseite des jeweiligen Gebietsverbands zu veröffentlichen. Das Ergebnisprotokoll beinhaltet mindestens:
a) die Ergebnisse von Abstimmungen mit Wortlaut der Anträge,
b) die Ergebnisse von Wahlen.
Das Protokoll ist von der Versammlungsleitung, der Wahlleitung und von der Protokollführung zu unterzeichnen und dem Vorstand des Gebietsverbands innerhalb von zwei Wochen zu übergeben. Die Protokolle sind für mindestens zehn Jahre zu archivieren.

7.9 Der Vorstand kann in zeitkritischen, wichtigen Fällen zu einer schriftlich zu begründenden, außerordentlichen Mitglieder-/Delegiertenversammlung einladen. Auf Kreis-/Ortsverbandsebene darf die Ladungsfrist 7 Tage nicht unterschreiten, auf Regional-/Landesebene darf die Ladungsfrist 14 Tage nicht unterschreiten, auf Bundesebene darf die Ladungsfrist 21 Tage nicht unterschreiten.


## § 8 Aufbau der Delegiertenversammlungen

8.1 Die Größe der Delegiertenversammlungen richtet sich nach der Anzahl der Mitglieder im jeweiligen Gebietsverband zum Beginn des laufenden Jahres. Die Anzahl der stimmberechtigten Delegierten eines Gebietsverbands beträgt grundsätzlich 10 % (kaufmännisch gerundet) der Mitgliederanzahl in diesem Gebietsverband. Führt dieses Verfahren zu einer Delegiertenversammlung mit mehr als 250 Personen, wird die Prozentzahl entsprechend nach unten korrigiert.

8.2 70 % (kaufmännisch gerundet) der stimmberechtigten Teilnehmenden der Delegiertenversammlung werden durch die Mitglieder des jeweiligen Gebietsverbands gewählt. 30 % (kaufmännisch gerundet) der teilnehmenden Delegierten werden vom jeweiligen Gebietsverband durch ein Losverfahren bestimmt. Für kurzfristige Absagen wird je eine Nachrücker*innen-Liste erstellt.

8.3 Jedes Parteimitglied kann bei seinem Parteieintritt angeben, ob es prinzipiell an diesem Losverfahren teilnehmen möchte. Jedes Parteimitglied kann seine Entscheidung bei der zuständigen Mitgliederverwaltung ändern. Jedes Parteimitglied, welches sein Einverständnis am Losverfahren teilzunehmen erklärt hat und nicht auf einer aktuellen Delegiertenliste steht, nimmt am Losverfahren teil.

8.4 Für die Organisation von Delegiertenwahl und Losverfahren sind die Vorstände der jeweiligen Gebietsverbände zuständig.


## § 9 Vorstände

9.1 Der Vorstand besteht aus Mitgliedern der Partei. Er führt die Geschäfte des jeweiligen Gebietsverbands nach Gesetz und Satzung und auf Grundlage der Beschlüsse der Parteiorgane. Der Vorstand vertritt die Partei politisch und organisatorisch nach innen und nach außen.

9.2 Jeder Vorstand besteht aus drei, fünf oder sieben Mitgliedern – darunter Vorsitzende\*r, stellvertretende\*r Vorsitzende\*r und eine für die Parteifinanzen verantwortliche Person. Über die genaue Anzahl der Vorstandsmitglieder entscheidet die Mitglieder-/Delegiertenversammlung des jeweiligen Gebietsverbands.

9.3 Die Mitglieder des Vorstands werden von der Mitglieder-/Delegiertenversammlung in geheimer Wahl grundsätzlich für die Dauer von zwei Jahren gewählt. Frühestens drei Monate vor und spätestens drei Monate nach Ablauf der zwei Jahre müssen Neuwahlen stattfinden. Es wird empfohlen, dass bei allen Vorstandswahlen mindestens ein Mitglied des Vorstands für vier Jahre gewählt wird.

9.4 Wenn der Vorstand eines Gebietsverbands aus weniger als drei Mitgliedern besteht oder es keine\*n Vorsitzende\*r, stellvertretende\*r Vorsitzende\*r oder Schatzmeister\*in mehr gibt, ist innerhalb von vier Wochen eine Mitglieder-/Delegiertenversammlung einzuberufen, die mindestens die Wahl neuer Vorstandsmitglieder beinhaltet. Hierzu lädt der Restvorstand in kommissarischer Funktion ein. Die in § 7.3 genannte Ladungsfrist ist einzuhalten. Der Restvorstand führt den Gebietsverband kommissarisch bis zu den Neuwahlen. Wenn ein Vorstand vollständig ausfällt, übernimmt der Vorstand des mitgliederstärksten Verbands der nächstniedrigeren Gliederung die kommissarische Leitung.

9.5 Die Mitglieder des Vorstands können von der Mitglieder-/Delegiertenversammlung insgesamt oder einzeln mit absoluter Mehrheit gewählt werden. Vorstandswahlen können nur stattfinden, wenn sie in der Einladung zur Mitglieder-/Delegiertenversammlung angekündigt worden sind.

9.6 Mitglieder des Vorstands und Bewerber\*innen für Vorstandsämter müssen ihre Mitgliedschaften in und Tätigkeiten für Aufsichtsräte, Vorstände, Verbände und Vereine gegenüber der Mitglieder-/Delegiertenversammlung offenlegen.

9.7 Die Kontoführung obliegt der\*dem Schatzmeister\*in. Mindestens ein weiteres Vorstandsmitglied hat ebenfalls eine Finanzvollmacht zu erhalten.

9.8 Der Vorstand führt bei seinen Sitzungen Protokoll. Die Protokolle sind für mindestens zehn Jahre zu archivieren.

9.9 Die Abwahl von einzelnen oder allen Vorstandsmitgliedern muss über einen regulären Antrag an die Mitglieder-/Delegiertenversammlung gestellt werden. Die in § 7.4 genannten Regularien sind einzuhalten.

9.10 Ist ein Vorstandsamt durch Rücktritt oder eine geheim abzustimmende Abwahl unbesetzt, so kann dieses von der Mitglieder-/Delegiertenversammlung durch Nachwahl neu besetzt werden. Die Amtszeit eines nachgewählten Vorstandsmitglieds geht bis zum turnusgemäßen Ende des durch die Nachwahl zu ersetzenden Vorstandmitglieds.

9.11 Ist ein Gebietsverband wiederholt nicht in der Lage zur Mitglieder-/Delegiertenversammlung einzuladen, obwohl alle nötigen Daten vorliegen, wird er aufgelöst und der nächsthöhere Gebietsverband übernimmt die Verwaltung der betroffenen Mitglieder.


## § 10 Wahlvorschläge

10.1 Gebietsverbände sind in ihrem Gebiet zur Einreichung von Wahlvorschlägen zu allgemeinen Wahlen berechtigt. Zur Aufstellungsversammlung lädt der Vorstand ein. Wenn ein Gebietsverband nicht existiert, kann der nächsthöhere existierende Gebietsverband die Wahlvorschläge einreichen.

10.2 Die Wahlvorschläge dürfen bei Listenwahlen maximal 50 % Männer enthalten. Die Plätze werden aufsteigend solange per Reißverschlussprinzip (abwechselnd Mann und Frau/divers oder umgekehrt) besetzt, bis ein Abwechseln der Geschlechter nicht mehr möglich ist.

10.3 Die Wahlvorschläge müssen von den beiden Vorstandsvorsitzenden des jeweiligen Gebietsverbands unterzeichnet werden.





