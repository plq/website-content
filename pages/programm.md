Title: PLQ Grundsatz Programm
Date: 2019-05-15 01:00:00
Author: PLQ Dresden
save_as: programm.html

<section id="Programm">
    <h1 style="margin-top:10px; margin-bottom:0px;">PLQ Grundsatz&shy;programm 2019</h1>
    <span><a href="downloads/Parteiprogramm_2019.pdf">PDF-Download</a></span>
    <br>

<h4 id="vorbemerkung">Vorbemerkung</h4>
<p style="font-style: italic;">Dies ist die erste Version unseres Parteiprogramms. Es soll kontinuierlich weiterentwickelt und kritisch hinterfragt werden. Wir bitten deshalb aktiv um <strong>Feedback</strong>. Welche Probleme haben wir übersehen und welche überbewertet? Welche Vorschläge sind zu zaghaft, welche zu weitgehend? Wir freuen uns über Rückmeldungen und ehrliche Meinung. Als Gründungsinitiative einer neuen Partei bieten wir aktiven Gestaltungsspielraum. Lasst uns diesen zum Wohle der Menschen nutzen!
</p>


<h2 id="Leitbild">
<a class="anchorlink" href="#Leitbild">
Unser Leitbild
</a>
</h2>


<p>Jeder Mensch hat das Recht auf ein menschenwürdiges Leben. Die Basis dafür ist die <strong>Erfüllung von Grundbedürfnissen</strong> wie Ernährung, Gesundheit, Wohnen, Sicherheit und soziale Teilhabe. Doch diese vermeintlichen Selbstverständlichkeiten sind nicht sicher: Zum einen bedroht das aktuelle Wirtschaftssystem die Lebensgrundlagen zukünftiger Generationen, zum anderen herrscht schon jetzt in weiten Teilen der Welt für viele Menschen Perspektivlosigkeit und Existenznot. Die weltweiten Entscheidungsstrukturen sind derzeit auf die Interessen der Wirtschaft und des Kapitalmarkts ausgerichtet. Unter dem &quot;Primat der Ökonomie&quot; bestimmen Staaten und transnationale Konzerne die Regeln zu ihrem ökonomischen Vorteil und dabei meist zu Lasten der Umwelt und der sozialen Gerechtigkeit. Das kapitalistische System zerstört damit seine eigenen Existenzgrundlagen. Politik muss dafür sorgen, dass die Welt ein lebenswerter Ort bleibt und die verfügbaren Ressourcen gerechter verteilt werden. Eine einseitig an wirtschaftlichem Wachstum orientierte Politik wird an beiden Zielen scheitern.</p>
<p></p>

<hr class="hr_gradient">
<a class="anchorlink" href="#politik_muss_dafuer_sorgen">
<div id="politik_muss_dafuer_sorgen" class="program_quote">
Politik muss dafür sorgen, dass die Welt
ein lebenswerter Ort bleibt und die verfügbaren Ressourcen gerechter verteilt werden.
</div>
</a>
<hr class="hr_gradient">

<p>Die PLQ tritt für eine Politik der Lebensqualität ein und setzt somit den Menschen in den Mittelpunkt. Wir möchten, dass Menschen weltweit – jetzt und in Zukunft – ein Leben in Würde führen können. Dazu gehört in erster Linie die Befriedigung von Grundbedürfnissen wie Ernährung, Gesundheit, Wohnen, Sicherheit und soziale Teilhabe. <strong>Lebensqualität</strong> ist aber mehr als die Befriedigung von Grundbedürfnissen. Lebensqualität bedeutet, ein gutes Leben zu führen. Dafür brauchen wir Zukunftsperspektiven, die uns optimistisch stimmen und uns Lebensfreude bringen. Wir brauchen Zeit und Ruhe und Privatsphäre – auch um uns zu überlegen, welches Leben wir führen wollen.</p>

<p>Um diese Ziele zu erreichen, bedarf es einer Gesellschaft, die nicht von permanentem Wachstum abhängig ist. Die Politik muss sich in erster Linie auf Mensch und Natur konzentrieren, nicht auf Wirtschaftsleistung und Arbeitsplätze. Was Mensch und Natur schadet, kann nicht gut sein. Wir treten für eine Gesellschaft ein, die das Wachstums- und Leistungsdogma konsequent überwindet und fordern daher eine sozial und ökologisch regulierte Marktwirtschaft. Lebensqualität muss zum zentralen Leitmotiv der Politik werden.</p>

<h2 id="die_zentralen_probleme">
<a class="anchorlink" href="#die_zentralen_probleme">
Die zentralen Probleme unserer Zeit
</a>
</h2>

<h3 id="oekologische_nicht-nachhaltigkeit">
<a class="anchorlink" href="#oekologische_nicht-nachhaltigkeit">
1. Ökologische Nicht-Nachhaltigkeit in Deutschland und Europa
</a>
</h3>

<p></p>

<p>Um den Klimawandel und den Raubbau an der Natur zu stoppen, fordern wir eine schnellstmögliche <strong>Umstellung auf erneuerbare Energien</strong>. Alle Kohle- und Atomkraftwerke auf dem Gebiet der EU müssen innerhalb weniger Jahre vom Netz. Stattdessen müssen Wind-, Sonnen- und Bioenergie stärker gefördert werden. Da nur noch wenige Flüsse in Deutschland unverbaut sind und sich vor allem kleine Wasserkraftwerke als ineffizient erwiesen haben, setzen wir uns innerhalb Deutschlands für die Renaturierung verbauter Flüsse und den Rückbau kleiner Wasserkraftwerke ein. Stattdessen sollen große Wasserkraftwerke modernisiert werden, um sie auf den aktuellen technischen und ökologischen Stand zu bringen. Da die flächendeckende Nutzung erneuerbarer Energien zurzeit durch fehlende Speichermöglichkeiten erschwert wird, setzen wir uns für ein Energiespeichergesetz ein, das die Entwicklung und Bereitstellung von Energiespeichermedien unabhängig von der Technologie finanziell fördert. Um dezentrale Energiekonzepte zu entwickeln, sollen Kommunen und Landkreise dabei unterstützt werden, einen lokalen und nachhaltigen Energiemix zu produzieren, der sie weitestgehend unabhängig von den großen Verbundnetzen macht.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#politik_muss_sich_in_erster_linie">
<div id="politik_muss_sich_in_erster_linie" class="program_quote">
Politik muss sich in erster Linie
auf Mensch und Natur konzentrieren,
nicht auf Wirtschaftsleistung und Arbeitsplätze.
</div>
</a>
<hr class="hr_gradient">


<p>Technologische Innovationen zur umweltschonenden Stromerzeugung sind unerlässlich, dürfen uns aber nicht davon ablenken, dass für eine zukunftsfähige Entwicklung eine generelle <strong>Senkung des Energieverbrauchs</strong> vonnöten ist. Der weltweite Energieverbrauch steigt seit Jahrzehnten kontinuierlich an. Vor allem in den Industrie- und Erdölländern ist der Pro-Kopf-Energieverbrauch erschreckend hoch und muss dringend gesenkt werden. Dafür schlagen wir – sowohl innerhalb Deutschlands als auch innerhalb der EU – eine spürbare Erhöhung der Energie- und Stromsteuer vor. Neben Strom, Benzin, Diesel, Erdgas, Flüssiggas, Schweröl und Kohle müssen in Zukunft auch Kerosin und Flugbenzin besteuert werden. Damit soll ein deutlicher Anreiz zum Energiesparen geschaffen werden.</p>

<p></p>

<p>Die Einnahmen dieser Steuern sollen dafür genutzt werden, den Produktionsfaktor Arbeit zu entlasten. Indem <strong>Energie teurer</strong> wird und <strong>Arbeit billiger</strong>, z. B. durch Senkung der Lohnnebenkosten, können wir nicht nur den Energieverbrauch in der industriellen Produktion senken, sondern auch dem absehbaren Arbeitsplatzverlust durch Automatisierung entgegenwirken. Im Zweifelsfall lohnt es sich dann eher, eine weitere Arbeitskraft einzustellen als eine neue Maschine anzuschaffen, die energieaufwändig produziert und betrieben werden muss. Auch weite Transportstrecken in der Herstellung und Vermarktung von Produkten würden sich ökonomisch nicht mehr rentieren. Für die Verbraucher*innen würde sich die Reparatur von defekten Geräten endlich wieder lohnen, weil sie deutlich günstiger wäre als ein Geräteneukauf. Da der Preis vieler Produkte durch die Erhöhung der Energie- und Stromsteuer steigen würde, sollen die Einnahmen außerdem dazu genutzt werden, die unteren Einkommensgruppen finanziell zu entlasten, z. B. durch Senkung der Einkommenssteuer. Langfristig sollen die <strong>Preise von Produkten</strong> sowohl die ökologischen als auch die sozialen Folgekosten ihrer Produktion widerspiegeln. Das heißt: Produkte, die unter ökologisch und sozial sinnvollen Bedingungen hergestellt wurden, sollen günstiger sein als konventionelle Produkte. Um dies zu gewährleisten, sollen Produkte mit einer guten Gemeinwohlbilanz steuerlich entlastet werden (Konzept der Gemeinwohl-Ökonomie).</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#produkte_die_unter_oekologisch">
<div id="produkte_die_unter_oekologisch" class="program_quote">
Produkte, die unter ökologisch und
sozial sinnvollen Bedingungen
hergestellt wurden, müssen günstiger
sein als konventionelle Produkte.
</div>
</a>
<hr class="hr_gradient">

<p></p>

<p>Um die Lebensmittelproduktion in Deutschland und Europa nachhaltiger zu gestalten, fordern wir einen sofortigen Stopp der Agrarsubventionen für Massentierhaltung und konventionelle Landwirtschaft. Öffentliche Fördermittel sollen in Zukunft nur an Betriebe vergeben werden, die nachweisbar ökologisch wirtschaften. Vor allem die Einhaltung der EU-Grenzwerte für Ammoniak und Nitrat muss in Zukunft stärker kontrolliert werden. Langfristig streben wir ein Verbot der Massentierhaltung innerhalb der EU sowie eine komplette <strong>Umstellung auf ökologische Landwirtschaftsmodelle</strong> an. Die innerhalb der EU erhältlichen Produkte müssen dann mindestens den Kriterien des heutigen EU-Bio-Siegels entsprechen. Insgesamt brauchen wir eine höhere Wertschätzung für unsere Lebensmittel. Außerdem setzen wir uns für eine massive Reduktion des Einsatzes von Agrar-Chemie ein, durch welche Probleme wie Insektensterben, multiresistente Keime und Grundwasserverschmutzung verursacht werden.</p>

<p>Im Bereich Mobilität setzen wir uns für die <strong>Förderung des Fahrrads und des öffentlichen Verkehrs</strong> ein. Innenstädte müssen weitestgehend autofrei gestaltet werden. Weniger Autos bedeuten auch weniger Staus, bessere Luft, weniger Lärm und weniger Stress. Um dieses Ziel zu erreichen, braucht es breite, baulich vom Autoverkehr getrennte Fahrradwege und einen attraktiveren Öffentlichen Personennahverkehr (ÖPNV). Busse und Bahnen im Innenstadtbereich müssen in vielen Städten deutlich häufiger fahren und intelligenter geplant werden. Auch der ländliche Raum würde von einem besser ausgebauten ÖPNV-System profitieren. Insgesamt muss der ÖPNV für alle Menschen finanziell erschwinglich sein. Um das Pendeln zwischen zwei nahegelegenen Städten zu erleichtern, sollen Fahrradschnellwege weiter ausgebaut werden. Auch im Fernverkehr müssen Busse und Bahnen attraktiver werden. Eine Fahrt mit Zug oder Fernbus sollte in jedem Fall günstiger sein als ein Kurzstreckenflug. Dafür müssen auch die bisherigen Subventionen fürs Fliegen abgeschafft werden. Um den Kraftstoffverbrauch im Individualverkehr zu senken sowie die Sicherheit auf den Straßen zu erhöhen, setzen wir uns zudem für ein Tempolimit von 130 km/h auf allen deutschen Autobahnen ein.</p>

<h3 id="gefaerdete_demokratie_in_deutschland">
<a class="anchorlink" href="#gefaerdete_demokratie_in_deutschland">
2. Gefährdete Demokratie in Deutschland und Europa
</a>
</h3>
<p></p>

<p>Unter Demokratie verstehen wir die Umsetzung des Mehrheitswillens unter Berücksichtigung von Minderheitenrechten. Beispiele aus der älteren und jüngeren Geschichte zeigen, dass diese gesellschaftliche Errungenschaft verschiedenen Gefahren ausgesetzt ist. Einerseits werden zu einfache Lösungen mit Absolutheitsanspruch mehrheitsfähig, andererseits ermöglichen moderne IT-Systeme die Überwachung und Kontrolle sowohl des Einzelnen als auch großer Bevölkerungsteile. Ein weiteres großes Problem ist die immer weiter zunehmende Konzentration von wirtschaftlicher Macht bei einigen wenigen Konzernen wie z. B. Banken, Investmentfonds und globale Aktiengesellschaften, und ihr Einfluss auf politische Prozesse ohne jede demokratische Legitimation. Dies geschieht z. B. durch Parteienfinanzierung, Einfluss auf Medien (Werbekunden) oder auch Korruption. Um die freiheitlich-demokratische Grundordnung gegenüber diesen Herausforderungen zu stärken, fordern wir u. a. eine Reform des Bildungssystems, der Sicherheitsbehörden, des Umgangs mit IT-Systemen, eine Wahlrechtsreform, eine deutlich verbesserte demokratische Legitimation der EU und ein strikteres Kartellrecht.</p>
<p></p>

<hr class="hr_gradient">
<a class="anchorlink" href="#ein_grosses_problem_ist">
<div id="ein_grosses_problem_ist" class="program_quote">
Ein großes Problem ist die immer
weiter zunehmende Konzentration
von wirtschaftlicher Macht bei
einigen wenigen Konzernen.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Unzweifelhaft wird die Welt immer komplexer und schwieriger zu verstehen. Deshalb sollte das <strong>Bildungssystem</strong> nicht primär der pragmatischen Berufsvorbereitung dienen, sondern Menschen die Fähigkeit zum selbstständigen und kritischen Denken vermitteln. Insbesondere im Umgang mit klassischen und sozialen Medien besteht aus unserer Sicht Nachholbedarf. Dadurch wird langfristig die Gefahr reduziert, dass einfache Scheinlösungen unsere grundlegenden demokratischen Errungenschaften existenziell bedrohen. Zudem sind Kompromissbereitschaft und Empathie unabdingbare Voraussetzungen für das Funktionieren einer demokratischen Gesellschaft und müssen dementsprechend im Bildungssystem gefördert werden.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#das_bildungssystem_sollte">
<div id="das_bildungssystem_sollte" class="program_quote">
Das Bildungssystem sollte nicht primär
der pragmatischen Berufsvorbereitung dienen,
sondern Menschen die Fähigkeit zum selbstständigen
und kritischen Denken vermitteln.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Des Weiteren gefährden auch <strong>unregulierte Datensammlungen</strong> und darauf aufbauende Algorithmen die Demokratie, z. B. indem sie die Mediennutzung und damit die demokratische Meinungs- und Willensbildung beeinflussen (Stichwort: Filterblasen). Deshalb fordern wir Transparenz bei der Datennutzung und -weitergabe sowie das Recht auf unkomplizierte Auskunft und Löschung von personenbezogenen Daten. Um <strong>Transparenz in den IT-Systemen</strong> insgesamt zu fördern, sollte möglichst viel freie und damit quelloffene Software verwendet werden, dies gilt insbesondere für öffentliche IT-Investitionen (Stichwort: Public Money – Public Code). Die ethische Frage, welche Entscheidungen durch Algorithmen überhaupt getroffen werden dürfen, wird in absehbarer Zeit hochrelevant. Darf beispielsweise ein Konzern auf Basis von Persönlichkeitsmodellen (Wahl-)Werbung auf einzelne Wähler*innen individuell zuschneiden? Um solche Fragen zu beantworten, setzen wir uns für einen breiten öffentlichen Diskurs mit staatlicher Unterstützung ein – mit dem Ziel, einen klaren Regulierungsrahmen für Algorithmen zu erarbeiten. Insgesamt sind die Möglichkeiten der Bürgerbeteiligung an der politischen Entscheidungsfindung, z. B. unter Nutzung digitaler Technologien, auszubauen.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#polizei_und_geheimdienste_nehmen">
<div id="polizei_und_geheimdienste_nehmen" class="program_quote">
Polizei und Geheimdienste nehmen
aus Perspektive der Demokratiesicherung
eine ambivalente Rolle ein.
</div>
</a>
<hr class="hr_gradient">

<p></p>

<p><strong>Polizei und Geheimdienste</strong> nehmen aus Perspektive der Demokratiesicherung eine ambivalente Rolle ein. Einerseits ist es ihre unverzichtbare Kernaufgabe, die Verfassung und die Gesetze zu schützen, andererseits sind sie auf Grund des staatlichen Gewaltmonopols strukturell anfällig für Machtmissbrauch. Deshalb fordern wir zum einen eine angemessene personelle und technische Ausstattung, um Straftaten durch Präsenz verhindern oder schnell aufklären zu können. Zum anderen fordern wir auch eine unabhängige Kontrolle und wirksame Sanktionsmöglichkeiten bei Fehlverhalten. Eine Erkennungsnummer an der Uniform sollte bei allen Einsätzen getragen werden. Verdeckte Operationen müssen durch unabhängige Verfahren streng reguliert und nach einer angemessenen Frist bzw. nach richterlicher Anordnung veröffentlicht werden. Verdachtsunabhängige staatliche Massenüberwachung, z. B. mit Hilfe von Gesichtserkennung und Bewegungsprofilen, lehnen wir ab. Durch unabhängige Kontrollinstanzen ist sicherzustellen, dass die Grundrechte der Bürger*innen gegenüber den staatlichen Organen gewahrt bleiben.</p>
<p>Wir betrachten den europäischen Einigungsprozess als große Errungenschaft. Allerdings weist die <strong>Europäische Union</strong> in ihrer aktuellen Form eine Reihe von Defiziten auf. Bisher orientiert sie sich einseitig an Wirtschaftsinteressen, was zu Recht zu Unmut und Akzeptanzproblemen in der Bevölkerung führt. Der Einfluss von Lobbyist*innen und insbesondere kommerziellen Interessenvertreter*innen ist deshalb vollständig transparent zu gestalten und sollte insgesamt eingeschränkt werden. Zusätzlich setzen wir uns für eine Wiederbelebung des EU-Verfassungsprozesses ein – mit dem Schwerpunkt auf bessere demokratische Legitimation der Institutionen und Gesetzgebungsverfahren. Erst wenn dieser Prozess abgeschlossen ist, können weitere nationalstaatliche Kompetenzen an die EU abgegeben werden.</p>
<p>Um unverhältnismäßig großen <strong>Einfluss von wirtschaftlichen Interessen</strong> auf politische Entscheidungen zu unterbinden, fordern wir einerseits deutlich mehr Transparenz und Kontrolle bei den Themen Parteienfinanzierung, Nebentätigkeiten und Lobbyismus. Andererseits sollte langfristig die Größe von Konzernen (und damit ihre Macht) durch ein strikteres Kartellrecht und eine stärkere Besteuerung begrenzt werden. Zudem setzen wir uns für internationale Regeln gegen ruinösen Deregulierungswettbewerb der Staaten untereinander ein.</p>
<p></p>
<h3 id="soziale_ungleichheit_in">
<a class="anchorlink" href="#soziale_ungleichheit_in">
3. Soziale Ungleichheit in Deutschland und Europa
</a>
</h3>

<p></p>

<p>Einkommen und Vermögen sind in Deutschland äußerst ungleich verteilt. Der Spitzensteuersatz ist seit 1953 von 95% auf 45% gesunken, während die Mehrwertsteuer seit 1968 von 10% auf 19% gestiegen ist. Wir fordern, dass die unteren Einkommensgruppen nicht mehr einseitig belastet werden und vom Reichtum der oberen Einkommensgruppen stärker profitieren. Für diese Umverteilung brauchen wir eine weitreichende <strong>Reform des Steuersystems</strong>. Insbesondere die Einkommenssteuer sollte nicht nur von der Höhe des Einkommens abhängig sein, sondern auch von der gesellschaftlichen Relevanz des jeweiligen Berufs. Wir brauchen eine offene Debatte darüber, welche Berufe für unsere Gesellschaft besonders wichtig sind und besser entlohnt werden müssen. Dementsprechend könnten z. B. Kranken- und Altenpfleger*innen, Reinigungskräfte, Feuerwehrmänner/-frauen, Arbeitskräfte in landwirtschaftlichen Betrieben, Lehrer*innen oder Erzieher*innen steuerlich entlastet werden. Zusätzlich fordern wir eine Finanztransaktionssteuer, um den Finanzsektor stärker an der Finanzierung des Gemeinwesens zu beteiligen und schädliche Spekulationen einzudämmen.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#sorgearbeit_die_meist_von_frauen">
<div id="sorgearbeit_die_meist_von_frauen" class="program_quote">
Sorgearbeit, die meist von Frauen
geleistet wird, muss in Deutschland
mehr Anerkennung erfahren.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Neben der bezahlten Erwerbsarbeit ist auch die sogenannte <strong>Sorgearbeit</strong> (Erziehung von Kindern, Pflege von Angehörigen) von enormer gesellschaftlicher Bedeutung. Diese Arbeit, die meist von Frauen geleistet wird, muss in Deutschland mehr Anerkennung erfahren. Auch dürfen für diejenigen, die Sorgearbeit leisten, keine finanziellen Nachteile entstehen. Wir fordern ein staatlich gezahltes, unbürokratisch zu beantragendes Mindesteinkommen für alle, die wegen Pflege oder Kindererziehung keinem regulären Job nachgehen können. Diejenigen, die nur noch in Teilzeit arbeiten können, sollen über eine geringere Einkommenssteuer entlastet werden. Außerdem soll die komplette Zeit der geleisteten Sorgearbeit spürbar auf die Rente angerechnet werden.</p>
<p>Zur <strong>Entlastung der unteren und mittleren Einkommensgruppen</strong> fordern wir eine Ausweitung des sozialen Wohnungsbaus, niedrigere Preise für ÖPNV und Kinderbetreuung sowie eine stärkere Kontrolle von Mindestlohn und Mietpreisbremse. Bei Nicht-Einhaltung der gesetzlichen Vorgaben müssen hohe Strafen gezahlt werden. Auch das System der Sozialversicherungen muss neu organisiert werden. Insbesondere die Beitragsbemessungsgrenzen bei der gesetzlichen Renten-, Arbeitslosen-, Kranken- und Pflegeversicherung müssen sofort abgeschafft werden, da sie die oberen Einkommensgruppen einseitig bevorzugen. Durch eine entsprechende Umverteilung können untere und mittlere Einkommensgruppen entlastet werden.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#jeder_mensch_hat_das_recht_auf">
<div id="jeder_mensch_hat_das_recht_auf" class="program_quote">
Jeder Mensch hat das Recht auf
ein Mindesteinkommen  unabhängig
von Alter, Behinderung,
Krankheit oder Erwerbsarbeit.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Jeder Mensch hat das Recht auf ein Mindesteinkommen – unabhängig von Alter, Behinderung, Krankheit oder Erwerbsarbeit. Das Zahlen einer <strong>Grundsicherung</strong> ist für uns eine gesellschaftliche Selbstverständlichkeit. Im derzeitigen System müssen Arbeitslose, Rentner*innen und Erwerbsgeminderte jedoch immer wieder als Bittsteller*innen auftreten und staatliche Leistungen hart erkämpfen. Diesen alltäglichen Erniedrigungen wollen wir ein Ende setzen. Alle Leistungen der Grundsicherung (Sozialgeld, Arbeitslosengeld II, Hilfe zum Lebensunterhalt, Grundsicherung im Alter und bei Erwerbsminderung) müssen unbürokratisch und zügig ausgezahlt werden. Sozialamt und Jobcenter müssen kundenfreundlich arbeiten. Der interne Sparzwang muss beendet werden.</p>
<p>Lebensqualität bedeutet auch, Zeit zu haben – Zeit für Familie, Freundeskreis, zivilgesellschaftliches Engagement oder Freizeitaktivitäten. Wir brauchen Zeit um zu überlegen, welche Träume und Ziele wir haben, welches Leben wir führen wollen und was uns glücklich macht. Vor allem brauchen wir Zeit, um zur Ruhe zu kommen, um durchzuatmen und uns nicht kaputt zu arbeiten. Besonders in den unteren Gehaltsgruppen fehlt es oft an dieser Zeit. Reinigungskräfte, Kassierer*innen und Küchenhilfen können es sich nicht leisten, in Teilzeit zu arbeiten – schon gar nicht, wenn sie eine Familie zu versorgen haben. Die mittleren und oberen Gehaltsklassen könnten sich zwar für weniger Erwerbsarbeit entscheiden, spüren aber oft den sozialen Druck zur Vollzeitbeschäftigung, wollen ihre Karriere nicht aufs Spiel setzen oder überschätzen ihre persönliche Leistungsfähigkeit. Die steigenden Burnout-Raten sind Ausdruck dieser Entwicklung. Wir glauben, dass alle Menschen ein Recht auf freie Zeit haben. Das Leben muss mehr als Erwerbsarbeit sein. Deshalb fordern wir die schrittweise <strong>Einführung der 30-Stunden-Woche</strong> in allen Berufsgruppen. Erst wenn die 30-Stunden-Woche zur gesellschaftlichen Normalität geworden ist, haben wir wieder Zeit für die wichtigen Dinge in unserem Leben. Der technische Fortschritt ermöglicht uns eine deutliche Reduktion der Arbeitsbelastung. Es liegt an uns, diese Chance im Sinne der Lebensqualität zu nutzen.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#wir_brauchen_zeit_um">
<div id="wir_brauchen_zeit_um" class="program_quote">
Wir brauchen Zeit, um zur Ruhe
zu kommen, um durchzuatmen
und uns nicht kaputt zu arbeiten.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Auch das deutsche Bildungssystem muss dringend reformiert werden. Weder die Ergebnisse der PISA-Studien noch die internationalen Rankings zur Chancengleichheit im Bildungssystem sind zufriedenstellend. Um auch sozial Schwächeren einen gesellschaftlichen Aufstieg zu ermöglichen, setzen wir uns deshalb für <strong>integrative und inklusive Ganztagsschulen</strong> und ein <strong>längeres gemeinsames Lernen</strong> ein. Die Trennung der Schüler*innen nach der 4. Klasse sowie die Aufteilung in Regel- und Förderschulen sind für uns unbefriedigende Zustände, die es zu überwinden gilt. Wir fordern eine 9-jährige verpflichtende Primärschule für alle, die Ablösung von Förderschulen durch die Einrichtung spezieller Förderklassen an regulären Schulen sowie eine bessere Ausstattung der Schulen mit entsprechendem Hilfspersonal. Insbesondere in den Bereichen Sozialarbeit, Sprachförderung und Sonderpädagogik braucht das reguläre Lehrpersonal erheblich mehr Unterstützung. Zudem muss die Aus- und Weiterbildung des regulären Lehrpersonals im Bereich Pädagogik verbessert werden. Die Schule darf kein Ort der reinen Faktenvermittlung sein, sondern muss Kinder zum eigenständigen reflektierten Denken motivieren und befähigen. Hier geht es auch um die Vermittlung vielfältiger sozialer Kompetenzen sowie die <strong>Bildung im Bereich Nachhaltigkeit und Antidiskriminierung</strong>. Inklusion (Einbeziehung von Kindern mit Behinderung) und Integration (Einbeziehung von Kindern mit Migrationshintergrund) sollen durch die Einrichtung spezieller Förderklassen an regulären Schulen sowie die Schaffung eines Kurssystems gewährleistet werden. In den meisten Fächern sollen Kurse mit verschiedenen Schwierigkeitsstufen angeboten werden, die den Schüler*innen je nach Leistungsniveau individuell zugewiesen werden.</p>


<hr class="hr_gradient">
<a class="anchorlink" href="#die_schule_darf_kein_ort">
<div id="die_schule_darf_kein_ort" class="program_quote">
Die Schule darf kein Ort der reinen
Faktenvermittlung sein, sondern muss
Kinder zum eigenständigen reflektierten
Denken motivieren und befähigen.
</div>
</a>
<hr class="hr_gradient">




<p>Um Eltern und Kinder bestmöglich zu fördern, setzen wir uns für ein <strong>flächendeckendes Beratungsangebot für Familien</strong> zur Vermittlung von psychologischen und pädagogischen Kompetenzen und Unterstützungsangeboten ein. Für diese Beratungen sollen externe Sozialarbeiter*innen an den Schulen als Ansprechpartner*innen zur Verfügung stehen. Um Kinderarmut vorzubeugen, fordern wir eine Bevorzugung von Alleinerziehenden bei der Vergabe von Kinderbetreuungsplätzen sowie eine steuerliche <strong>Entlastung Alleinerziehender</strong> über Senkung der Einkommenssteuer im unteren Lohnbereich. Außerdem fordern wir die Abschaffung des Ehegattensplittings, um eine Kindergrundsicherung von 500 € pro Kind und Monat finanzieren zu können. Die 500 € <strong>Kindergrundsicherung</strong> werden auf eventuelle Sozialleistungen nicht angerechnet. Neben finanzieller Unterstützung braucht es auch mehr kostenlose Freizeit- und Vereinsangebote für Familien. Insbesondere Sportvereine sollten stärker staatlich bezuschusst werden. Mit diesen Maßnahmen möchten wir erreichen, dass Deutschland für Familien wieder attraktiv wird.</p>


<hr class="hr_gradient">
<a class="anchorlink" href="#alle_in_deutschland_lebenden">
<div id="alle_in_deutschland_lebenden" class="program_quote">
Alle in Deutschland lebenden
Menschen brauchen eine angemessene
medizinische Versorgung.
</div>
</a>
<hr class="hr_gradient">


<p>Die derzeitigen Freiwilligendienste (Freiwilliges Soziales Jahr, Freiwilliges Ökologisches Jahr, Bundesfreiwilligendienst) sollen im <strong>Allgemeinen Gesellschaftsdienst</strong> (AGD) zusammengelegt werden. Freiwillige, die sich für den AGD entscheiden, leisten 10 Monate Unterstützungsarbeit in gesellschaftlich relevanten Bereichen und bekommen anschließend zwei Monate bezahlten Urlaub. AGD-Freiwillige werden bei der Vergabe von Studien- und Ausbildungsplätzen bevorzugt. Außerdem wird das AGD-Jahr doppelt auf die Rente angerechnet. Die Kosten für den AGD werden gemeinsam von Bund und Arbeitgebern getragen.</p>
<p>Im Bereich Gesundheit fordern wir eine <strong>solidarische Bürgerversicherung</strong> für alle. Private und gesetzliche Krankenversicherung sollen in einem gemeinsamen Modell vereinigt werden. Für die Beiträge zur solidarischen Bürgerversicherung werden sämtliche Einkunftsarten (Löhne, Gehälter, Zinsen, Dividenden, Tantiemen, Miet- und Pachterlöse) herangezogen. Um allen in Deutschland lebenden Menschen eine angemessene medizinische Versorgung zur Verfügung zu stellen, wird der Leistungskatalog im Vergleich zur bisherigen gesetzlichen Krankenversicherung erweitert. Insbesondere präventive Gesundheitsleistungen (speziell für gefährdete Berufsgruppen) und alternative Heilmethoden sollen stärker gefördert werden.</p>



<hr class="hr_gradient">
<a class="anchorlink" href="#die_gesellschaftliche_tendenz_zur">
<div id="die_gesellschaftliche_tendenz_zur" class="program_quote">
Die gesellschaftliche Tendenz zur
Privatisierung und Ökonomisierung hat
insbesondere im Gesundheitssystem
massive negative Auswirkungen.
</div>
</a>
<hr class="hr_gradient">


<p>Die in vielen gesellschaftlichen Bereichen stattfindende <strong>Tendenz zur Privatisierung und Ökonomisierung</strong> hat insbesondere im Gesundheitssystem massive negative Auswirkungen. Vor allem die als Aktiengesellschaften organisierten Klinikgruppen arbeiten in der Regel gewinnorientiert und vernachlässigen die Bedürfnisse der Patient*innen. Auch freigemeinnützige Träger (Kirchen, Wohlfahrtsverbände) arbeiten zunehmend gewinnorientiert. Wir fordern, dass alle privaten Krankenhäuser wieder in eine öffentliche Trägerschaft zurückgeführt werden. Von allen freigemeinnützigen Trägern verlangen wir einen öffentlich zugänglichen Geschäftsbericht sowie die Achtung von Tarifverträgen und grundlegenden Arbeitnehmerrechten.</p>
<p>Die derzeitigen <strong>Abrechnungssysteme im stationären und ambulanten Bereich</strong> (DRG-System, Budgetierung) führen nicht nur zu einer hohen Belastung des medizinischen Personals mit bürokratischen Aufgaben, sondern begünstigen auch medizinische Fehlbehandlungen (Über- oder Unterversorgung). Wir fordern feste Gehälter für alle niedergelassenen Ärzt*innen und alle Angestellten in öffentlichen Krankenhäusern sowie mehr Personal in Krankenhäusern durch feste Personal-Patienten-Schlüssel. Zusätzlich sollen alle notwendigen medizinischen Leistungen (ohne Budgetgrenzen) von der Krankenkasse bezahlt werden. Dies verhindert, dass sich einzelne Ärzt*innen oder Klinikbetreiber finanziell bereichern oder an Leistungen gespart wird, die medizinisch notwendig sind. Um unnötige Operationen zu vermeiden, sollen für alle nicht-akuten invasiven Eingriffe unabhängige Zweitmeinungen eingeholt werden.</p>
<p>Um die <strong>Pflegeberufe</strong> attraktiver zu machen, braucht es auch in den Alten- und Pflegeheimen einen festen Personalschlüssel. Zusätzlich sollen alle Pflegekräfte durch die Einführung der 30-Stunden-Woche und den verstärkten Einsatz von nicht-professionellem Unterstützungspersonal entlastet werden. Das Hilfspersonal soll über den AGD gestellt werden. Um möglichst vielen Menschen ein würdiges Altern zu ermöglichen, müssen <strong>alternative Wohn- und Pflegekonzepte</strong> (wie z. B. Seniorensiedlungen, Mehrgenerationenhäuser oder das niederländische Buurtzorg-Modell) erprobt werden.</p>


<hr class="hr_gradient">
<a class="anchorlink" href="#in_den_letzten_jahren">
<div id="in_den_letzten_jahren"  class="program_quote">
In den letzten Jahren hat sich die
Gesellschaft bezüglich Migrationsfragen
stark gespalten. Wir streben eine
Überwindung dieser Spaltung an.
</div>
</a>
<hr class="hr_gradient">



<p>In den letzten Jahren hat sich die Gesellschaft bezüglich Migrationsfragen stark gespalten. Wir streben eine Überwindung dieser Spaltung an. Das Phänomen der irregulären Migration halten wir sowohl für die betroffenen Migrant*innen als auch für die beteiligten Länder für hochproblematisch. Um unserer humanitären Verantwortung gerecht zu werden, setzen wir uns deshalb dafür ein, <strong>geregelte Fluchtmöglichkeiten </strong>nach Deutschland zu schaffen. Ein Asylantrag soll in Zukunft sowohl in allen deutschen Botschaften als auch in den Flüchtlingslagern des UNHCR möglich sein. Potenzielle Antragsteller*innen sollen dabei realistisch über Risiken und Chancen einer Migration nach Deutschland aufgeklärt werden. Mit diesen Maßnahmen kann verhindert werden, dass Menschen mit geringer Bleibeperspektive den gefährlichen Weg nach Europa antreten. Zusätzlich wollen wir das deutsche Kontingent der aufzunehmenden Geflüchteten beim UNHCR deutlich erhöhen. Damit soll auch die Planbarkeit der Geflüchtetenzahlen verbessert werden und so kann der Integrationsprozess sofort beginnen. Neben politisch Verfolgten und Opfern gewaltsamer Konflikte sollen auch Betroffene schwerer humanitärer Katastrophen ein Recht auf Asyl bekommen, falls sie in der Region nicht ausreichend versorgt werden können. Zusätzlich muss der Etat des UNHCR deutlich erhöht werden.</p>



<hr class="hr_gradient">
<a class="anchorlink" href="#wir_verfolgen_eine_aktive">
<div id="wir_verfolgen_eine_aktive" class="program_quote">
Wir verfolgen eine aktive
Integrationspolitik und binden
Neuzugewanderte in die
Organisation ihrer Umgebung mit ein.
</div>
</a>
<hr class="hr_gradient">



<p>Während im Bereich Flucht humanitäre Gründe ausschlaggebend sind, sollte sich die <strong>Regulierung der Arbeitsmigration</strong> an den Erfordernissen des heimischen Arbeitsmarktes orientieren. Die Erteilung von Arbeitssuchvisa sollte erleichtert werden. Vor der Erteilung erfolgt eine umfassende und realistische Beratung über die Arbeits- und Lebensperspektiven in Deutschland. Um einem sogenannten &quot;Brain Drain&quot; entgegenzuwirken, ist mit den Arbeitsministerien des jeweiligen Landes zusammenzuarbeiten. Bei Zustandekommen eines Arbeitsvertrages werden Arbeitsmigrant*innen zu einem Sprach- und Integrationskurs verpflichtet.</p>
<p>Wir verfolgen eine <strong>aktive Integrationspolitik</strong> und binden Neuzugewanderte in die Organisation ihrer Umgebung mit ein. Darunter verstehen wir konkret: bessere Aufklärung in Bezug auf ihre Rechte, lokale Mitbestimmung und feste Ansprechpartner*innen bei Problemen – denn nur ein Zuhause, das ich gestalten kann, kümmert mich. Um Missverständnisse, Unsicherheiten und daraus resultierende Konflikte zu vermeiden, fordern wir eine Umstrukturierung der bestehenden Integrationskurse. Neben der deutschen Sprache und Geschichte sollten Migrant*innen dort auch Informationen zur Alltagskultur erhalten. Insbesondere Bürgerrechte und -pflichten müssen dabei eine zentrale Rolle spielen. Einerseits kann die Niederlassung von Menschen aus anderen Kulturkreisen eine enorme kulturelle Bereicherung darstellen, andererseits auch erhebliche Spannungen verursachen. Wir setzen uns dafür ein, dass die Mehrheitsgesellschaft weltoffen und tolerant gegenüber Menschen anderer Herkunft ist. Selbstverständlich fordern wir von allen Menschen, die in Deutschland leben wollen, dass sie sich an die hiesigen Gesetze halten, die Rechte und Bedürfnisse ihrer Mitmenschen sowie die freiheitlich-demokratische Grundordnung anerkennen.</p>



<hr class="hr_gradient">
<a class="anchorlink" href="#es_gibt_keinen_absoluten_mangel">
<div id="es_gibt_keinen_absoluten_mangel" class="program_quote">
Es gibt keinen absoluten Mangel an
öffentlichen Gütern – wir haben es
nur mit einem wachsenden
Umverteilungsproblem zu tun.
</div>
</a>
<hr class="hr_gradient">


<p>Um <strong>Konkurrenzsituationen</strong> wegen knapper Güter (Wohnungen, Plätze in Kitas und Schulen, Arzttermine) zu vermeiden, brauchen wir erhebliche Investitionen in Bildung, Kinderbetreuung, Wohnraum und das Gesundheitssystem. Wir finden, dass Arme nicht gegen Arme ausgespielt werden sollten. Es gibt keinen absoluten Mangel an öffentlichen Gütern – wir haben es nur mit einem wachsenden Umverteilungsproblem zu tun.</p>
<p>Auch auf europäischer Ebene brauchen wir mehr Solidarität. Die <strong>Wohlstandsgefälle innerhalb der Europäischen Union</strong> sind gravierend, die derzeitigen Mechanismen der Umverteilung unzureichend. Wir müssen dafür sorgen, dass alle Menschen innerhalb der EU ein menschenwürdiges Leben führen können. Das heißt: Ausbau der sozialen Sicherungssysteme, Zugang zu grundlegender medizinischer Versorgung und Bildung, Zugang zu bezahlbarem Wohnraum sowie Schaffung hochwertiger Arbeitsplätze. Die EU darf nicht nur Hüterin von Wettbewerb und offenen Märkten sein, sie muss auch für soziale Gerechtigkeit in ihren Mitgliedsländern sorgen. Erst wenn alle Bürger*innen der EU eine angemessene Rente, ein angemessenes Arbeitslosengeld, würdige Arbeitsbedingungen sowie ausreichenden Zugang zu Bildung, Gesundheit und Wohnraum haben, wird die EU von der breiten Bevölkerung akzeptiert werden. Solange sie Großkonzerne schützt, Steueroasen toleriert, marode Banken rettet und einzelnen Ländern unsoziale Sparprogramme auferlegt, wird sie keine politische Zukunft haben.</p>
<p></p>
<p></p>

<h3 id="globale_ungerechtigkeit">
<a class="anchorlink" href="#globale_ungerechtigkeit">
4. Globale Ungerechtigkeiten
</a>
</h3>

<p></p>

<p>Während die Menschen innerhalb der Europäischen Union in Frieden leben, stehen bewaffnete Konflikte in anderen Teilen der Erde auf der Tagesordnung. Ohne Frieden und Sicherheit ist es unmöglich, einen demokratischen Staat und ein funktionierendes Wirtschaftssystem aufzubauen. Die EU muss deshalb ihren Schwerpunkt auf die <strong>Verhinderung und Beilegung bewaffneter Konflikte</strong> legen. Sie muss außerdem sorgfältig analysieren, inwiefern ihre eigene Handels-, Außen- und Sicherheitspolitik zur Verfestigung von Gewalt in anderen Ländern führt. Neben Diplomatie und dem sofortigen Stopp von Waffenlieferungen in Krisengebiete werden auch faire Handelsabkommen und die Unterstützung der lokalen Wirtschaft vonnöten sein. Die besten Friedensabkommen sind brüchig, solange die vor Ort lebenden Menschen keine ökonomische Zukunftsperspektive haben.</p>
<p>Auch unabhängig von bewaffneten Konflikten brauchen wir <strong>faire Handelsabkommen</strong> zwischen Industrie-, Schwellen- und Entwicklungsländern. Wir können nicht länger dabei zusehen und daran mitwirken, wie für unseren Wohlstand andere Länder ausgebeutet werden. Viele Produkte, die innerhalb der EU erhältlich sind, können nur deshalb so billig sein, weil in den Herkunftsländern soziale und ökologische Standards ignoriert werden. Dies hat massive Auswirkungen auf das Leben der lokalen Bevölkerung: Kinder gehen nicht zur Schule, weil sie arbeiten müssen. Eltern verdienen einen Hungerlohn, mit dem sie kaum ihre Familie ernähren können. Multinationale Unternehmen kaufen in großem Stil landwirtschaftliche Flächen auf und rauben den lokalen Bauern ihre Lebensgrundlage. Boden, Wasser und Luft werden verschmutzt durch unzureichende Umweltauflagen. Fehlender Arbeitsschutz gefährdet die Gesundheit der lokalen Arbeiter*innen. Wir fordern <strong>verbindliche Umwelt- und Fair-Trade-Standards</strong> für alle innerhalb der EU erhältlichen Produkte. Die Importeure haben dabei die Nachweispflicht. Sie müssen zeigen, dass die von ihnen eingeführten Güter unter sozialen und ökologischen Bedingungen produziert wurden. Auch die Spekulation mit Boden und Nahrungsmitteln muss ein Ende haben. Die <strong>Finanztransaktionssteuer</strong> kann dazu einen entscheidenden Beitrag leisten.</p>



<hr class="hr_gradient">
<a class="anchorlink" href="#wir_koennen_nicht_laenger_dabei_zusehen">
<div id="wir_koennen_nicht_laenger_dabei_zusehen" class="program_quote">
Wir können nicht länger dabei
zusehen und daran mitwirken, wie
für unseren Wohlstand andere
Länder ausgebeutet werden.
</div>
</a>
<hr class="hr_gradient">



<p>Um den sozialen und ökologischen Wandel weltweit zu fördern, setzen wir uns für einen <strong>globalen Informationsaustausch</strong> zwischen Unternehmen, zivilgesellschaftlichen Organisationen und politischen Institutionen ein. Dieser sollte durch entsprechende EU-Programme unterstützt werden. Austausch und gegenseitige Beratung sollen dabei helfen, die Fehler, die andere gemacht haben, nicht selbst zu wiederholen. Gelungene Projekte können im eigenen Land wiederholt oder angepasst werden. Ganz gleich, ob es sich um die Entwicklung eines Elektromotors, eines Wahlsystems oder eines Friedensabkommens handelt: Gegenseitige Unterstützung ist wichtig und verhindert oftmals große Fehlentwicklungen. Wir dürfen uns hier nicht länger voneinander abschotten. Das bedeutet auch, dass wir in Zukunft viel offener mit Wissen umgehen müssen. Betriebs- und Geschäftsgeheimnisse sowie das Patentsystem müssen vor diesem Hintergrund kritisch hinterfragt werden.</p>
<p>Gelingt es uns, die Folgen des Klimawandels zu minimieren, einen fairen Handel zu etablieren, die Macht multinationaler Unternehmen einzuschränken sowie gewaltsame Konflikte beizulegen, brauchen wir keine klassische <strong>Entwicklungshilfe</strong> mehr. Auch ein Großteil der aktuellen Fluchtbewegungen könnte somit verhindert werden. Natürlich sollte die EU weiterhin lokale Organisationen unterstützen (z. B. in den Bereichen Friedensarbeit, Armutsbekämpfung, Bildung, soziale Gerechtigkeit, Klima und Umwelt). Sie sollte aber keineswegs von oben herab über Hilfsprogramme entscheiden, die an den strukturellen Problemen vorbeigehen und ihre eigene Machtposition unhinterfragt lassen. Auch hier ist ein Dialog auf Augenhöhe nötig.</p>
<p></p>


<h3 id="fazit">
<a class="anchorlink" href="#fazit">
Fazit
</a>
</h3>

<p></p>
<p>Die Menschheit sieht sich im 21. Jahrhundert sowohl immensen Problemen als auch enormen Chancen gegenüber. Die derzeitigen wirtschaftlichen und politischen Konzepte sind Relikte der Vergangenheit, die die Probleme zum großen Teil verursacht haben. Eine Lösung ist von ihnen nicht zu erwarten. Das gegenwärtige Wirtschaftsmodell, das auf beständiges Anwachsen von Konsum und Produktion abzielt und dabei unvermeidlich Menschen und Natur schadet, kann nicht dauerhaft bestehen. In den aktuellen politischen Entscheidungsstrukturen wird das zerstörerische Potenzial des ungezügelten Kapitalismus schlicht ignoriert. Dies halten wir für rücksichtslos und unverantwortlich. Eine realistische Utopie, d. h. ein wirklich zukunftsfähiges politisches Gesamtkonzept, fehlt bislang.</p>

<hr class="hr_gradient">
<a class="anchorlink" href="#das_gegenwaertige_wirtschaftsmodell">
<div id="das_gegenwaertige_wirtschaftsmodell" class="program_quote">
Das gegenwärtige Wirtschaftsmodell,
das auf beständiges Anwachsen
von Konsum und Produktion abzielt,
kann nicht dauerhaft bestehen.
</div>
</a>
<hr class="hr_gradient">

<p></p>
<p>Wenn die Errungenschaften der Zivilisation erhalten und sogar ausgebaut werden sollen, ist eine <strong>sozial-ökologische Transformation der Gesellschaft</strong> unumgänglich. Die Vorschläge und Forderungen in diesem Programm dienen dazu, eine solche Transformation anzustoßen. Sie stellen einige vermeintliche Gewissheiten in Frage, etwa dass das Schaffen von Arbeitsplätzen per se ein politischer Erfolg ist. Zudem enthält das Programm durchaus unbequeme Punkte, wie z. B. eine spürbare Erhöhung der Energiepreise. Um es klar und deutlich zu sagen: Wir halten es sowohl für notwendig als auch erstrebenswert, dass weniger gekauft, geflogen und gearbeitet wird. Notwendig aus simpler Einsicht in die Naturgesetze und erstrebenswert aus der Erkenntnis heraus, dass Arbeit und Konsum nicht der Sinn und Zweck eines erfüllten Lebens sind. Was zählt, ist die Lebensqualität.</p>
<p>Damit die Transformation nicht zu einem Ansteigen sondern zu einem Rückgang sozialer Ungerechtigkeit führt, enthält das Programm zahlreiche Ausgleichs- und Umverteilungsmaßnahmen. Anerkennung von Sorgearbeit, Preissenkung des ÖPNV und eine Verlagerung der Steuerlast von Erwerbsarbeit zu Ressourcenverbrauch, Kapitaleinkommen und Vermögen sind dafür erste Schritte.</p>
<p>Die große Herausforderung für die Demokratie besteht darin, für diejenigen Maßnahmen Mehrheiten hervorzubringen, die zwar unpopulär, aber für eine positive Entwicklung notwendig sind. <strong>Lebensqualität</strong> ist dabei der Maßstab, an dem sich alle politischen Entscheidungen auszurichten haben.</p>
<p></p>

<h4 id="schlussbemerkung">
<a class="anchorlink" href="#schlussbemerkung">
Schlussbemerkung (=Vorbemerkung)
</a>
</h4>

<p style="font-style: italic;">Dies ist die erste Version unseres Parteiprogramms. Es soll kontinuierlich weiterentwickelt und kritisch hinterfragt werden. Wir bitten deshalb aktiv um <strong>Feedback</strong>. Welche Probleme haben wir übersehen und welche überbewertet? Welche Vorschläge sind zu zaghaft, welche zu weitgehend? Wir freuen uns über Feedback und ehrliche Meinung. Als Gründungsinitiative einer neuen Partei bieten wir aktiven Gestaltungsspielraum. Lasst uns diesen zum Wohle der Menschen nutzen!
</p>

<p>
<em>Schreibt uns an:</em><br><br>
    <img src="./img/contact-mail.png" alt="Email-Adresse: kontakt at domain-der-webseite.de">

<br><br>
<em>oder direkt hier:</em><br>


    <form action="scripts/process.php" method="post">

            <input name="meta-type" type="hidden" value="opinion-form" />


            Text (max. 5000 Zeichen):<br>
            <textarea name="field2" cols="50" rows="12">Ich denke ... </textarea>
            <br>
            <br>

            E-Mail-Adresse (zum Antworten, optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>

            <input type="submit" name="submit" value="Absenden">
    </form>

</p>

</section>


<!-- the following opening tag serves to fix a strange behavior in pelican -->
<p>
