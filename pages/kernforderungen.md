Title: Kernforderungen
Date: 2021-01-25 17:30:00
Author: PLQ Dresden
save_as: kernforderungen.html
status: hidden


<!-- Diese Markdowndatei wird vom Website-Bau-Programm "zerhackt" und dort eingefügt wo wir die kernforderungen wollen.
Als Trennmarkierung fungiert der als nächstes folgende spezielle Kommentar (`< !-- ++++++++++++ -- >`):

Die Überschrift (beginnend mit ##) bezeichnet die "Klappbox-Überschrift" und ggf. den Slider-Text.
Alles Unter der Überschrift kommt in den Aufklappbaren Teil.

Kernforderungen tauchen nur im Slider auf, wenn sie einen Kommentar `< !-- slider: True -- >` enthalten.


Die Option `slug` ist eine Zeichenkette (alphanumerisch + "_") die für den direktlink auf eine Forderung benutzt wird.


Gendersternchen muss mit "\*" geschrieben werden, weil es sonst von Markdown als Syntaxelement für *kursiven* Text aufgefasst wird


-->


<!-- ++++++++++++ -->

## Menschenwürde ernst nehmen: Faire Asyl- und  Integrationspolitik!
<!-- slider: True -->
<!-- slug: menschenwuerde_ernst_nehmen -->

Grundsätzlich gilt: Wir setzen uns für eine Welt ein, in der die Beschränkung der Freizügigkeit durch nationale Grenzen überflüssig ist. Die Schaffung gleichwertiger Lebensverhältnisse aller Menschen ist unser Ziel. Die grundsätzliche Politik der PLQ hat die Bekämpfung klassischer Fluchtursachen wie humanitäre Notlagen oder Menschenrechsverletzungen als Ziel. Diese Maßnahmen wirken aber erst langfristig.

Irregulären Migration ist sowohl für die betroffenen Menschen als auch für die beteiligten Länder hochproblematisch. Aus grundsätzlicher humanitärer Verantwortung und in Anerkennung der Verantwortung für den Kolonialismus müssen Deutschland und Europa wesentlich mehr tun, um Menschen in Not zu helfen. Wir fordern konkret: 1. Der der Etat des UNHCR muss deutlich erhöht werden, um schnelle Hilfe in betroffenen Regionen leisten zu können. 2. Ein Asylantrag soll in Zukunft sowohl in allen deutschen Botschaften als auch in den Flüchtlingslagern des UNHCR möglich sein. Potenzielle Antragsteller*innen sollen dabei realistisch über Risiken und Chancen einer Migration nach Deutschland aufgeklärt werden. Zusätzlich wollen wir 3. das deutsche Kontingent der aufzunehmenden Geflüchteten beim UNHCR deutlich erhöhen. Damit soll auch die Planbarkeit der Geflüchtetenzahlen verbessert werden und so kann der Integrationsprozess sofort beginnen. Neben politisch Verfolgten und Opfern gewaltsamer Konflikte sollen auch Betroffene schwerer humanitärer Katastrophen ein Recht auf Asyl bekommen, falls sie in der Region nicht ausreichend versorgt werden können.

Perspektivlosigkeit, Traumatisierung und teilweise problematische Sozialisationen verursachen Konflikte und Probleme, die sich durch Ignoranz und Desinteresse noch ausweiten können. Wir fordern daher eine strukturell verankterte Willkommenskultur und eine aktive Integrationspolitik, die es Zugewanderten ermöglicht, selbstbestimmt an der Gesellschaft teilzuhaben und sie mitzugestalten. Missverständnissen, Unsicherheit und eventuellen Konflikten muss durch Aufklärung, Hilfsangebote und klare Perspektiven proaktiv entgegengetreten werden. Wir setzen uns dafür ein, dass die Mehrheitsgesellschaft weltoffen und tolerant gegenüber Menschen anderer Herkunft ist. Selbstverständlich fordern wir von allen Menschen, die in Deutschland leben, dass sie sich an die Gesetze halten, die Rechte und Bedürfnisse ihrer Mitmenschen respektieren sowie die freiheitlich-demokratische Grundordnung anerkennen.


<!-- ++++++++++++ -->

## Agrarsubventionen nur noch für nachhaltige Landwirtschaft!

<!-- slider: True -->
<!-- slug: nachhaltige_argrarsubventionen -->

Um den nachhaltigen Umbau der Landwirtschaft zu fördern ist eine Neuausrichtung der Agrarsubventionen notwendig. Die bisher auf grenzenloses Wachstum ausgerichtete Politik, hat auch im Agrarberereich keine Zukunft: Die Intensivierung der Landwirtschaft sorgt durch Überdüngung und Pestizideneinsatz nicht nur für den Rückgang der biologischen Vielfalt, sondern belasten auch unsere Luft, Böden und Gewässer. Die unfaire Auszahlung einer pauschalen Flächenprämie begünstigt vor allem Großbetriebe ganz unabhängig von ihren Umwelt- und Tierschutzleistungen. Somit werden nicht nur Anreize zur Überproduktion geschaffen, es profitieren insbesondere Großbetriebe. Deswegen fordern wir einen sofortigen Stopp von umweltschädlichen Agrarsubventionen. Des Weiteren trägt die Industrielle Massentierhaltung nicht nur erheblich zum Außstoß von klimaschädlichen Treibhausgasen bei, sondern bedeutet auch unendliches Leid. Unterstützung soll nur umwelt- und tiervertäglichen Wirtschaftsweisen zugute kommen, um die notwendige nachhaltige Transformation der Landwirtschaft mit Förderung ökologisch zu steuern und auch sozial abzufedern. Hiermit verbunden ist eine stärkere Regionalisierung landwirtschaftlicher Märkte. Potenziale zum Aufbau und Förderung (bestehender) regionaler Wirtschaftkreisläufe sollen genutzt werden um durch kürzere Wege den Transportaufwand und die damit verbundenen verkehrsbedingten Umweltbelastungen zu reduzieren aber auch um die Artenvielfalt zu erhalten.

<!-- ++++++++++++ -->

## Diskriminierung überwinden und Gleichberechtigung fördern!

<!-- slider: True -->
<!-- slug: diskriminierung_ueberwinden -->

Gesellschaftliche Benachteiligung auf Grund von Geschlecht, Herkunft, Alter oder anderen (zugeschriebenen) Merkmalen ist ein gravierendes Problem und mit dem Prinzip *Lebensqualität für alle* nicht vereinbar. Daher begreifen wir Antidiskriminierung als dringend notwendige zivilisatorische Aufgabe, die auf struktureller Ebene angegangen werden muss. Konkret fordern wir

- Qualifizierte und niederschwellige Gleichstellungsbeauftragte deren Zuständigkeitsbereich alle öffentlichen Einreichtungen und insbesondere das Bildungs- und Justizsystem abdeckt.
- Integrative und inklusive Ganztagsschulen sowie längeres gemeinsames Lernen
- Bildung im Bereich Toleranz und Antidiskriminierung
- Bessere Anerkennung von Sorgearbeit z.B. bei der Rente

Um Kinderarmut vorzubeugen, fordern wir eine Bevorzugung von Alleinerziehenden bei der Vergabe von Kinderbetreuungsplätzen sowie eine steuerliche Entlastung Alleinerziehender über Senkung der Einkommenssteuer im unteren Lohnbereich. Außerdem fordern wir die Abschaffung des Ehegattensplittings und stattdessen eine Kindergrundsicherung von 500 € pro Kind und Monat, die auf eventuelle Sozialleistungen nicht angerechnet wird.


<!-- ++++++++++++ -->

## Nachhaltigkeit – klare Regeln statt Moralappelle!

<!-- slider: True -->
<!-- slug: klare_nachhaltigkeits_regeln -->

Knallharter Wettbewerb zwingt Unternehmen oft, so billig wie möglich zu produzieren - auch wenn Klimaschutz und faire Löhne dabei auf der Strecke bleiben. Statt Moralappelle an den "bewussten Kund:innen" fordern wir umwelt- und fairtrade-gerechte Produktionsstandards, am besten europaweit. Deren Einhaltung wird durch den Staat geprüft, nicht durch jede einzelnen Kundin. Beispiele für Anforderungen sind:

- Nahrungsmittel - sowohl heimische als auch importierte - müssen dem beutigen "Bio"-Standard entsprechen, also nachhaltig und tierfreundlich produziert werden
- Gesamte Produktionskette frei von Kinderarbeit und Ausbeutung
- Geräte müssen energiesparsam sein und eine angemessene Garantiezeit haben (einschließlich Recht auf Reparatur)
- möglichst wenig Verpackung, Verpackung recyclebar

Diese Standards werden innerhalb einiger Jahre schittweise eingeführt, um plötzliche "Schocks" in der Wirtschaft zu vermeiden. Damit eventuelle Preiserhöhungen nicht zu Lasten ärmerer Menschen gehen, wird das Sozialsystem entsprechend reformiert (siehe [Programm](/programm.html)).

Siehe auch: Michael Kopatz, "Ökoroutine - Damit wir tun, was wir für richtig halten".


<!-- ++++++++++++ -->

## 4-Tage-Woche – Zeitwohlstand statt Hamsterrad!
<!-- slider: True -->
<!-- slug: zeitwohlstand_statt_hamsterrad -->

Produktion wird effizienter: Immer weniger Arbeit wird für dieselbe Menge Waren benötigt. Bisher schafft Wirtschaftswachstum Ausgleich, wir produzieren und konsumieren immer mehr, und die Nachfrage nach Arbeit bleibt ungefähr gleich. Die Politik sieht sich gezwungen, Arbeitsplätze zu fördern, auch wenn mehr Produktion der Umwelt schadet und zumindest die Wohlhabenden unter uns eigentlich mehr als genug konsumieren können, und eher über Zeitmangel denn über materiellen Mangel klagen. Mit anderen Worten, wir müssen immer mehr konsumieren, um Arbeitslosigkeit zu verhindern!
Kürzere Arbeitszeiten – zum Beispiel eine 4-Tage-Woche als Standard – entschärft das Problem. Die Arbeit wird besser verteilt, Arbeitslosigkeit verringert, Produktion gedrosselt, und wer nun unter der Arbeit stöhnt, bekommt Gelegenheit, nicht-materielle Freuden zu genießen oder sich ehrenamtlich zu engagieren: Zeitwohlstand. Um zu verhindern, dass Niedrigbezahlte unter den kürzeren Arbeitszeiten zu leiden haben, muss entweder der Mindestlohn angehoben oder anderweitig finanzieller Ausgleich geschaffen werden.


