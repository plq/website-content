Title: Index
Date: 2017-11-05 18:49:00
Author: PLQ Dresden
save_as: {{save_as_name|default("index.html")}}
css_file: slider{{context.boxes|length}}.css

<!-- this comment will be recognized by unittests utc_landing_page -->
<section id="sec-Idee">
    <h1>Partei für Lebensqualität</h1>

<div id=slidewrapper>
<p style="padding-left: 5px;">Kernforderung:</p>
<div id=slideset>

{% for box in context.boxes %}
  <div>
    <a href="#ford_{{box.slug}}"> {{box.title}}</a>
  </div>
{% endfor %}
</div>
</div>


<a name="idee" class="shifted_anchor"></a>
    <h3>Die Idee</h3>

    <p>
    Gegenwärtig werden politische Entscheidungen oft mit Wettbewerbsfähigkeit,
    Wachstum und Arbeitsplätzen begründet und fallen oft zu Gunsten ökonomischer Interessen aus.
    Politik sollte sich aber am Erhalt und der Verbesserung der Lebensqualität aller Menschen
    (jetzt und zukünftig sowie lokal und global) orientieren.
    Die Initiative zur Gründung einer "Partei für Lebensqualität" ist der Versuch,
    sich <i>konstruktiv und anschlussfähig</i> mit den großen Herausforderungen
    (z. B. soziale Spaltung, Klimawandel, Gewalt, Ressourcenknappheit, globale Ungleichheit,
    Digitalisierung) auseinanderzusetzen und dabei den politischen Diskurs möglichst positiv zu beeinflussen.
    </p>

    <p>
    Unser Wertegerüst und Politikansatz lässt sich folgendermaßen zusammengefassen:
    </p>

    <a name="werte" class="shifted_anchor"></a>
    <ul>
        <li>
        Problemlösung statt Menschenfeindlichkeit
        </li>
        <li>
        Nachhaltigkeit statt Wachstumsdogma
        </li>
        <li>
        Solidarität statt Rücksichtslosigkeit
        </li>
        <li>
        Zeitwohlstand statt Hamsterrad
        </li>
        <li>
        Selbstbestimmung statt Perspektivlosigkeit
        </li>
        <li>
        Gute Argumente statt hohler Phrasen
        </li>
        <li>
        Dialog statt Provokation
        </li>
        <li>
        Gerechtes Wirtschaften statt Neoliberalismus
        </li>
        <li>
        Menschenrechte vor Profit
        </li>
        <li>
        Grundbedürfnisse vor Luxus
        </li>
    </ul>

    <p>
    Mehr Details finden sich in unserem <a href="programm.html">Grundsatzprogramm</a>.
    </p>
<hr>
</section>

<section id="sec_Kernforderungen">
<a name="kernforderungen" class="shifted_anchor"></a>
<h3>Konkrete Forderungen</h3>
<p>
Unsere politischen Vorstellungen sind im <a href="programm.html">PLQ-Grundsatzprogramm</a> umfassend dargestellt. Daraus ergeben sich folgende konkrete Forderungen (Klicken für Details):
</p>

{% for box in context.boxes %}
    <a name="ford_{{ box.slug}}" class="shifted_anchor"></a>
    <details>
    <summary>{{ box.title_txt }}</summary>
    <div class="ford_details">{{ box.body }}</div>
    </details>
{% endfor %}

</section>


<section id="ja-aber">
<h3>Häufig gestellte Fragen:</h3>

{% for box in context.faq_boxes %}
    <a name="faq_{{ box.slug}}" class="shifted_anchor"></a>
    <details>
    <summary>{{ box.title_txt }}</summary>
    <div class="ford_details">{{ box.body }}</div>
    </details>
{% endfor %}

<hr>
</section>
<section id="was-nun">
<h3>Was nun?</h3>
    <ul>
        <li>
        <a href="#newsletter">Newsletter</a>
        </li>
        <li>
        <a href="#soziale-netzwerke">Soziale Netzwerke</a>
        </li>
        <li>
        <a href="#unterstuetzen">Unterstützen</a>
        </li>
        <li>
        <a href="#mitmachen">Mitmachen</a>
        </li>
        <li>
        <a href="#ortsgruppen">Ortsgruppen</a>
        </li>
        <li>
        <a href="#feedback">Deine Meinung</a>
        </li>
    </ul>
<!-- <p>&nbsp;</p> -->
<hr>
</section>
<section id="info-mailingliste-section">
<a name="newsletter" class="shifted_anchor"></a>
 <h2>Newsletter</h2>
    <p> Zielgruppe des Newsletters (Info-Mailingliste) sind Menschen, die die Idee erst mal interessant finden und schauen möchten, was daraus wird.
    Mit der knappen Ressource Aufmerksamkeit gehen wir sparsam um:
    Das Mailaufkommen beträgt im Durchschnitt weniger als eine Mail im Monat.
    Eine Austragung ist jederzeit möglich.
    Und natürlich haben wir strenge (und kurze) <a href="impressum.html#Datenschutz">Datenschutzbestimmungen</a>.
    </p>
<!--      -->
    <form action="/d/scripts/process.php" method="post">
<!--              -->
            <input name="meta-type" type="hidden" value="newsletter-form" />
<!--      -->
            E-Mail-Adresse:<br>
            <div class="input-field"><input name="field2" type="text" /></div> <br>
<!--              -->
            Ort/Region (optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>
<!--              -->
            <div class="input-field">
            <input type="radio" name="action" value="in" checked> Eintragen
            <input type="radio" name="action" value="out"> Austragen
            </div>
            <br>
            <input type="submit" name="submit" value="Absenden">
    </form>
<!--      -->
<hr>
</section>
<section id="sec-soziale-Netzwerke">
<a name="soziale-netzwerke" class="shifted_anchor"></a>
<h2>Soziale Netzwerke</h2>
    <p>
    Wir sind auch auf sozialen Netzwerken aktiv, wobei eine
    Vorbemerkung aus unserer Sicht unumgänglich ist, insbesondere bezüglich Facebook:
    </p>
<!--      -->
    <div class="citation">
    Facebook ist in den vergangenen Jahren durch zahlreiche zweifelhafte Geschäftspraktiken in die Kritik geraten.
    Digitale Sweatshops auf den Philippinen <a href="http://www.sueddeutsche.de/digital/internetzensur-wer-die-blutlachen-bei-facebook-aussortiert-1.2969438">[1]</a>,
    massive Steuervermeidung in Europa <a href="http://www.spiegel.de/wirtschaft/unternehmen/facebook-will-mehr-steuern-zahlen-a-1080714.html">[2]</a>,
    mangelhafter Datenschutz für die Nutzer*innen <a href="http://www.sueddeutsche.de/digital/facebook-und-co-in-der-kritik-gierig-nach-daten-1.22950">[3]</a>
    und Funktionen, die auch das Sammeln von Informationen über Nichtmitglieder ermöglichen <a href="http://www.faz.net/aktuell/feuilleton/medien/soziale-netzwerke-facebook-weiss-alles-ueber-uns-1936297.html">[4]</a>,
    sind nur einige von vielen Beispielen. Auch wir haben lange überlegt, ob wir die Dienste dieses Unternehmens in Anspruch nehmen wollen.
    Wie im sonstigen Leben geht es auch in der Politik darum, Kompromisse zu finden. Niemand kann seinen eigenen Prinzipien zu 100% treu bleiben.
    Bevor wir allerdings vor lauter schlechtem Gewissen gar nichts tun und in der Bedeutungslosigkeit versinken,
    wollen wir es zumindest einmal versuchen - mit Facebook, mit Zähneknirschen, aber auch mit Vorfreude auf die vielen Menschen und Initiativen,
    mit denen wir uns über diesen Kanal vernetzen können.
    Und falls wir irgendwann einmal an den Schalthebeln der Macht sitzen, werden wir uns selbstverständlich dafür einsetzen,
    dass man Dienstleistungen wie Facebook auch ohne schlechtes Gewissen nutzen kann...
    </div>
    <p>
    Eine Alternative zu Facebook und Twitter ist das  <a href="https://de.wikipedia.org/wiki/Freie_Software">freie</a> dezentrale soziale Netzwerk Mastodon*.
    </p>
    <ul>
        <li>
        <a href="https://legal.social/@plq">Mastodon</a>  (<a href="https://en.wikipedia.org/wiki/Mastodon_(software)">Hintergrundinfos</a>)
        </li>
        <li>
        <a href="https://twitter.com/plqde">Twitter</a>
        </li>
        <li>
        <a href="https://www.facebook.com/Partei-f%C3%BCr-Lebensqualit%C3%A4t-1405801329497001">Facebook</a>
        </li>
    </ul>
<hr>
</section>
<section id="unterstuetzen-section">
<a name="unterstuetzen" class="shifted_anchor"></a>
    <h2>Unterstützen</h2>

    <p>
    Die "PLQ-Gründungsinitiative" lebt bisher vom Engagement einiger weniger junger Menschen.
    Das betrifft sowohl die inhaltliche und organisatorische Arbeit als auch den finanziellen Aufwand.
    Mit diesem Modell sind wir ein Stück gekommen, aber um der Idee wirklich Schwung zu verleihen, brauchen wir
    mehr Köpfe, mehr Hände und auch mehr Portmonees.
    Ohne Budget kann man weder Öffentlichkeitsarbeit machen, noch Veranstaltungen durchführen oder Strukturen aufbauen.
    </p>

    <p>
    Wenn Du also keine Zeit oder Möglichkeit hast, aktiv mitzumachen, unseren Ansatz aber trotzdem unterstützen möchtest,
    würden wir uns über eine Förderung von z. B. 5,- Euro monatlich oder eine einmalige Spende freuen.
    Einerseits gibt uns das Handlungsoptionen, andererseits ist es ein Signal für uns, dass wir auf dem richtigen Weg sind.
    </p>
    <p>
    Hinweis: Noch sind wir weder eine anerkannte Partei noch ein Verein. D.h. wir können auch keine Spendenquittung ausstellen.
    </p>

    <form action="scripts/process.php" method="post">

            <input name="meta-type" type="hidden" value="donation-form" />

            Name (optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>

            E-Mail-Adresse:<br>
            <div class="input-field"><input name="field2" type="text" /></div> <br>

            Betrag:<br>
            <div class="input-field"><input name="field3" type="text" /></div><br>

            <div class="input-field">
            <input type="radio" name="field4" value="once" checked> Einmalig
            <input type="radio" name="field4" value="montly"> Monatlich (Widerruf jederzeit möglich)
            </div>

            <input type="submit" name="submit" value="Absenden">
    </form>

    <p>
    Das ausgefüllte Formular fassen wir als Absichtserklärung und Planungsgrundlage auf und verschicken dann zweimal im Jahr eine Zahlungserinnerung.
    Ein Rücktritt kann formlos per E-Mail erfolgen. Es gelten unsere <a href="impressum.html#Datenschutz">Datenschutzbestimmungen</a>.
    </p>

<hr>
</section>
<section id="mitmachen-section">
<a name="mitmachen" class="shifted_anchor"></a>
<h2>Mitmachen</h2>
    <p>
    Das Projekt "PLQ-Gründungsinitiative" steht noch ziemlich am Beginn.
    Es finden wöchentlich Telekonferenzen statt und es gibt regelmäßige überregionale Treffen.
    Dazu verfügen wir über grundlegende digitale Infrastruktur und arbeiten am Aufbau von
    Regionalgruppen (siehe unten).
    </p>
    <p>
    Das ist ein Anfang, aber um spürbar positiven Einfluss auszuüben, brauchen wir mehr Leute, Ressourcen und Struktur.
    Deswegen: Falls Du mit unserer Grundidee im Wesentlichen übereinstimmst, dann schreibe uns und verbreite diesen Aufruf in Deinem Bekanntenkreis.

    Wir freuen uns über Interesse, Anregungen, <a href="#unterstuetzen">Unter&shy;stützung</a> und auch konstruktive Kritik.
    Am meisten freuen wir uns natürlich über Menschen, die bei uns mitmachen wollen...
    </p>
    <!--  -->
    <div style="text-align:center; font-size:130%; color:#000;">
    <!-- kontakt{ät}plq:de -->
    <a name="mail" class="shifted_anchor"></a>
    <img src="./img/contact-mail.png" alt="Email-Adresse: kontakt at domain-der-webseite.de">
    </div>
    <p> Falls Du unverbindlich auf dem Laufenden gehalten werden möchtest, kannst Du
    uns in <a href="#soziale-netzwerke">sozialen Netzwerken</a> folgen oder den <a href="#newsletter">Newsletter</a> (durchschnittlich weniger als eine Mail im Monat)
    abonnieren.
    </p>
<hr>
</section>
<section id="ortsgruppen-section">
<a name="ortsgruppen" class="shifted_anchor"></a>
    <h2>Ortsgruppen</h2>
    <p>
    Derzeit sind in mehreren Regionen Ortsgruppen in der Gründungsphase.
    Bei Interesse schreib einfach eine kurze Mail an die jeweils verantwortlichen Personen.
    </p>
    <ul>
<!--        <li>
        Berlin:  berlin{ät}plq.de
        </li>-->
        <li>
        Dresden: dresden{ät}plq.de
<!--        <ul>
            <li>
            Nächstes Treffen: ... 18:30 Uhr,
            <a href="http://www.ladencafe.de/aha_kontakt.html">aha-Café</a> (Kreuzstraße)
            </li>
        </ul>-->
        </li>
<!--        <li>
        Freiburg: freiburg{ät}plq.de
        </li>-->
        <li>
        Marburg: marburg{ät}plq.de
        </li>
    </ul>
    <p>
    Wenn Du in Deiner Region gerne eine Gruppe gründen möchtest, sag einfach <a href="#mail">per E-Mail</a> Bescheid.
    </p>
<hr>
</section>
<section id="feedback-section">
<a name="feedback" class="shifted_anchor"></a>
    <h2>Sag uns Deine Meinung</h2>

    <form action="d/scripts/process.php" method="post">

            <input name="meta-type" type="hidden" value="opinion-form" />


            Text (max. 5000 Zeichen):<br>
            <textarea name="field2" cols="50" rows="8">Ich denke ... </textarea>
            <br>
            <br>

            E-Mail-Adresse (zum Antworten, optional):<br>
            <div class="input-field"><input name="field1" type="text" /></div> <br>

            <input type="submit" name="submit" value="Absenden">
    </form>

</section>


<!-- the following opening tag serves to fix a strange behavior in pelican -->
<p>
