Title: Impressum
Date: 2017-02-16 18:49:00
Author: PLQ Dresden
save_as: impressum.html

<section id="Impressum">
    <h1>Impressum</h1>
    <h3>Postanschrift</h3>
    
    <!--
    <p>
    PLQ-Gründungsinitiative<br>
    c/o <br>
    H 6<br>
    0 n<br>
    </p>
    -->

    <p> 
    <img src="./img/address.png" alt="Adresse">
    </p> 
<hr>
    <p> 
    
    <img src="./img/contact-mail.png" alt="Adresse">
    </p> 
    <p> 
    <a href="http://plq.de">http://plq.de</a>
    </p> 
<hr>
</section>
<section id="Haftung-section">
<a name="Haftung" class="shifted_anchor"></a> 
    
    <h3>Haftung für Links</h3>
    <p>    
    Wir übernehmen keine Verantwortung für die Inhalte fremder Angebote.
    Seiten, auf die wir durch externe Links unmittelbar verweisen, werden von uns
    vor dem Setzen eines Links sorgfältig ausgewählt. Auf spätere Änderungen von
    unmittelbar verlinkten Seiten sowie auf Inhalte nachfolgender Seiten fremder
    Angebote haben wir jedoch keinen Einfluss. Wir distanzieren uns daher ausdrücklich
    von späteren Änderungen unmittelbar verlinkter Seiten, den Inhalten auf 
    nachfolgenden Seiten sowie deren Anbietern. 
    </p>    
    
    <hr>
</section>
<section id="Datenschutz-section">
<a name="Datenschutz" class="shifted_anchor"></a> 
    <h3>Datenschutzbestimmungen</h3>
    <p>
    Die PLQ tritt für das Prinzip der <a href="https://de.wikipedia.org/wiki/Datenvermeidung_und_Datensparsamkeit"><b>Datensparsamkeit</b></a> ein.
    Erhobene Daten (E-Mail-Adressen, Spendenzusagen) werden:
    </p>
    <ul>
        <li>
         nur für den vorgesehen Zweck verwendet (Newsletter, Spendenabwicklung),
        </li>
        <li>
        nur auf Rechnern gespeichert, die den Datenschutzgesetzen der Bundesrepublik unterliegen,
        </li>
        <li>
        nicht an Dritte weitergegeben,
        </li>
        <li>
        nach bestem Wissen und Gewissen vor dem Zugriff Dritter geschützt (verschlüsselte Übertragung und Archivierung)
        </li>
    </ul>
    

</section>
